export default [
    {
        "value": "mr",
        "label": "Mr."
    },
    {
        "value": "ms",
        "label": "Ms."
    },
    {
        "value": "mrs",
        "label": "Mrs."
    }
]
