import React from "react";
import {createBottomTabNavigator, createStackNavigator,} from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import {defaultNavigationOptions, Routes, TabBarOptions} from "./Config";

export  const HomeBottomTab = createMaterialBottomTabNavigator(Routes,{
    initialRouteName:'Home',
    ...TabBarOptions
});

export default createStackNavigator({ HomeBottomTab },{
    defaultNavigationOptions,
    initialRouteName:'HomeBottomTab',
} );
