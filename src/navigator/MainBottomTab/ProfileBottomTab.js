import React from "react";
import {createBottomTabNavigator, createStackNavigator} from "react-navigation";
import {defaultNavigationOptions, Routes, TabBarOptions} from "./Config";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import HomeBottomTab from "./HomeBottomTab";


const ProfileBottomTab = createMaterialBottomTabNavigator(Routes,{
        initialRouteName:'Profile',
        ...TabBarOptions
});

export default createStackNavigator({ ProfileBottomTab},{
    defaultNavigationOptions
} );
