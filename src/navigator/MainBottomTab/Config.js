import React from "react";
import NotificationScreen from "../../screens/NotificationScreen";
import {TouchableOpacity, View} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Material from "react-native-vector-icons/MaterialIcons";
import HomeScreen from "../../screens/HomeScreen";
import ProfileScreen from "../../screens/ProfileScreen";
import JobSearchScreen from "../../screens/JobSearchScreen";
import BookmarksScreen from "../../screens/BookmarksScreen";
import HeaderWithBurger from "../../components/Navigation/HeaderWithBurger";
import {createStackNavigator} from 'react-navigation'
import JobSingle from "../../screens/JobSingle";
import ProfileEditScreen from "../../screens/ProfileEditScreen";
import CvScreen from "../../screens/CvScreen";
import CustomIcon from "../../utils/CustomIcon";
import {translate} from "../../utils/translations";

export const HomeStack = createStackNavigator({HomeScreen, JobSingle}, {
    mode: 'modal',
    headerMode: 'none',
});
export const JobSearchStack = createStackNavigator({JobSearchScreen, JobSingle}, {
    mode: 'modal',
    headerMode: 'none',
});
export const BookmarksScreenStack = createStackNavigator({BookmarksScreen, JobSingle}, {
    mode: 'modal',
    headerMode: 'none',
});
export const ProfileStack = createStackNavigator({ProfileScreen, ProfileEditScreen,CvScreen}, {
    headerMode: 'none',
});
const FONT_SIZE = 20;
export const Routes = {
    Home: {

        screen: HomeStack, navigationOptions: () => ({
            title: translate('home'),
            headerBackTitle: null,
            tabBarIcon: ({tintColor}) => <CustomIcon name="house" size={FONT_SIZE} color={tintColor}/>,

        }),
    },
    Profile: {

        screen: ProfileStack, navigationOptions: () => ({
            title: translate('profile'),
            headerBackTitle: null,
            tabBarIcon: ({tintColor}) => <CustomIcon name="user" size={FONT_SIZE + 5} color={tintColor}/>,

        }),
    },
    JobSearchScreen: {

        screen: JobSearchStack, navigationOptions: () => ({
            title: translate('job_history'),
            headerBackTitle: null,
            tabBarIcon: ({tintColor}) => <CustomIcon name="briefcase" size={FONT_SIZE} color={tintColor}/>,

        }),
    },
    BookmarksScreen: {

        screen: BookmarksScreenStack, navigationOptions: () => ({
            title: translate('bookmarks'),
            headerBackTitle: null,
            tabBarIcon: ({tintColor}) => <CustomIcon name="like-heart" size={FONT_SIZE} color={tintColor}/>
        }),
    },
    Notification: {

        screen: NotificationScreen, navigationOptions: () => ({
            title: translate('notifications'),
            headerBackTitle: null,
            tabBarIcon: ({tintColor}) => <CustomIcon name="notification" size={FONT_SIZE + 2} color={tintColor}/>,
            badge:110
        }),
    },
};


export const defaultNavigationOptions = ({navigation}) => {
    return ({
        title: navigation.state.key,
        header: (props) => <HeaderWithBurger {...props} />,
        headerLeft: (
            <TouchableOpacity
                style={{
                    height: 44,
                    width: 44,
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
                onPress={() => navigation.openDrawer()}>
                <FontAwesome name="bars" size={20}/>
            </TouchableOpacity>
        ),

    })
}
export const TabBarOptions = {
    activeColor: '#005E9D',
    inactiveColor: '#A3A3A3',
    barStyle: {backgroundColor: '#ffffff'},
    labeled: true,
    shifting: true
}
