import React from "react";
import {
    createAppContainer,
    createStackNavigator,
} from "react-navigation";
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import AppDrawerNavigator from "./AppDrawerNavigator";
import {
    createReduxContainer,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
import {connect} from "react-redux";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import CodeScreen from "../screens/CodeScreen";
import ResetPassword from "../screens/ResetPasswordScreen";


const AuthNavigator = createStackNavigator({
    Login: LoginScreen,
    Register: RegisterScreen,
    Forgot: ForgotPasswordScreen,
    CodeScreen: {
        screen:CodeScreen,
        navigationOptions: {
            gesturesEnabled: false,
        },
    },
    ResetPassword:{
        screen:ResetPassword,
        navigationOptions: {
            gesturesEnabled: false,
        },
    },
},{  headerMode: 'none',});

const RootNavigator = createAnimatedSwitchNavigator({
        Auth:AuthNavigator,
        Home:AppDrawerNavigator,
    },{
    initialRouteName:'Auth',
    // backBehavior:'history',
    transition: (
        <Transition.Together>
            <Transition.Out
                type="slide-bottom"
                durationMs={400}
                interpolation="easeInOut"
            />
            <Transition.In type="fade" durationMs={500} />
        </Transition.Together>
    ),
});
const middleware = createReactNavigationReduxMiddleware(
    state => state.nav,
    'root',
);
const AppWithNavigationState = createReduxContainer(RootNavigator, 'root');
const mapStateToProps = state => ({
    state: state.nav,
});

const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);
export { RootNavigator, AppNavigator, middleware };
