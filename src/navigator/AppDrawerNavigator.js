import React from "react";
import {createDrawerNavigator, createStackNavigator} from "react-navigation";
import NotificationScreen from "../screens/NotificationScreen";
import BlogScreen from "../screens/BlogScreen";
import ContactUsScreen from "../screens/ContactUsScreen";
import {ImageBackground, KeyboardAvoidingView, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import HomeBottomTab from "./MainBottomTab/HomeBottomTab";
import {screenHeight} from '../utils/Styles'
import {DrawerNavigatorItems} from '../components/Navigation/DrawerNavigatorItems'
import CustomIcon from "../utils/CustomIcon";
import JobSearchScreen from "../screens/JobSearchScreen";
import JobSingle from "../screens/JobSingle";
import HeaderWithBurger from "../components/Navigation/HeaderWithBurger";
import BlogSingleScreen from "../screens/BlogSingleScreen";
import {translate} from "../utils/translations";

const JobSearchScreenStack = createStackNavigator({JobSearchScreen, JobSingle}, {
    // headerMode:'none',
    mode: 'modal',
    defaultNavigationOptions: ({ navigation }) => ({
        header: (props) => <HeaderWithBurger {...props} />,
    })

})
const BlogStack = createStackNavigator({BlogScreen, BlogSingleScreen}, {
    // headerMode:'none',
    mode: 'modal',
    defaultNavigationOptions: ({ navigation }) => ({
        header:null,
    })

})
const AppDrawerNavigator = createDrawerNavigator({
    HomeBottom: {
        screen: HomeBottomTab,navigationOptions: () => ({
            title: translate('home'),
            headerBackTitle: null,
            drawerIcon: ({tintColor}) => <CustomIcon name="house" size={22} color={tintColor}/>
        }),
    },

    Blog: {
        screen: BlogStack, navigationOptions: () => ({
            title: translate('blog'),
            headerBackTitle: null,
            drawerIcon: ({tintColor}) => <CustomIcon name="blog" size={22} color={tintColor}/>,
        }),
    },
    ContactUs: {
        screen: ContactUsScreen, navigationOptions: () => ({
            title: translate('contact_us'),
            headerBackTitle: null,
            drawerIcon: ({tintColor}) => <CustomIcon name="phone-contact" size={22} color={tintColor}/>,

        }),
        activeTintColor: 'red'
    },

}, {
    hideStatusBar: false,
    contentComponent: props => (<View style={{flex: 1, marginTop: -6}}>
        <SafeAreaView style={{flex: 1}} forceInset={{top: 'always', horizontal: 'never'}}>
            <ImageBackground source={require('../components/Navigation/img/menuGradient.png')}
                             style={{flex: 1, width: "100%", height: screenHeight}}>
                <KeyboardAvoidingView style={{flex: 1, paddingRight: 10}} enabled behavior="padding">
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        maxHeight: 100,
                        flexDirection: 'row'
                    }}>
                        <TouchableOpacity onPress={() => props.screenProps.changeLanguage('hy')}><Text
                            style={[{color: '#fff'}, props.screenProps.lang === 'hy' && {fontWeight: 'bold'}]}>ARM</Text></TouchableOpacity>
                        <TouchableOpacity onPress={() => props.screenProps.changeLanguage('en')} style={{marginLeft: 10}}><Text
                            style={[{color: '#fff'}, props.screenProps.lang === 'en' && {fontWeight: 'bold'}]}>ENG</Text></TouchableOpacity>
                        <TouchableOpacity style={{marginLeft: 10}} onPress={() => props.screenProps.changeLanguage('ru')}><Text style={[{
                            color: '#fff'},
                            props.screenProps.lang === 'ru' && {fontWeight: 'bold'}
                        ]}>RUS</Text></TouchableOpacity>
                    </View>
                    <DrawerNavigatorItems {...props} />
                </KeyboardAvoidingView>

            </ImageBackground>
        </SafeAreaView>

    </View>),
    drawerBackgroundColor: 'transparent',
    overlayColor: 70,
    defaultNavigationOptions: ({navigation}) => {
        // console.log(navigation)
        return ({
            title: navigation.state.key,
            headerLeft: (
                <TouchableOpacity
                    style={{
                        height: 44,
                        width: 44,
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    onPress={() => navigation.openDrawer()}>
                    <Icon name="bars" size={20}/>
                </TouchableOpacity>
            ),

        })
    },
    contentOptions: {
        itemStyle: {
            borderBottomWidth: 0.3,
            borderBottomColor: '#FFFFFF'
        },
        activeTintColor: '#FFFFFF',
        inactiveTintColor: '#FFFFFF',
        itemsContainerStyle: {
            marginVertical: 0,
        },
        iconContainerStyle: {
            opacity: 1,
        }
    },
});

export default AppDrawerNavigator;
