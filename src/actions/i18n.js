export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';


export const changeLanguage = (payload:Object) =>({
    type:CHANGE_LANGUAGE,
    payload
})
