export const SET_BOOKMARK = 'SET_BOOKMARK';
export const SET_INITIAL_BOOKMARK = 'SET_INITIAL_BOOKMARK';
export const REMOVE_BOOKMARK = 'REMOVE_BOOKMARK';

export const setBookmark = payload =>({
    type:SET_BOOKMARK,
    payload
});

export const removeBookmark = payload =>({
    type:REMOVE_BOOKMARK,
    payload
})
export const setInitialBookmarks = payload =>({
    type:SET_INITIAL_BOOKMARK,
    payload
})
