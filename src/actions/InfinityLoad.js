export  const ON_FETCH_START = 'infinityLoad/FETCH_START';
export  const ON_FETCH_SUCCESS = 'infinityLoad/FETCH_SUCCESS';
export  const ON_FETCH_FAIL = 'infinityLoad/FETCH_FAIL';
export  const ON_LOAD_MORE = 'infinityLoad/LOAD_MORE';
export  const ON_REFRESH = 'infinityLoad/REFRESH';
export  const ON_SEARCH_START = 'infinityLoad/SEARCH_START';
export  const ON_SEARCH_SUCCESS = 'infinityLoad/SEARCH_SUCCESS';


export const fetchStart = () => ({
    type:ON_FETCH_START
});
export const fetchSuccess = payload => ({
    type:ON_FETCH_SUCCESS,
    payload
});
export const loadMore = payload => ({
    type:ON_LOAD_MORE,
    payload
});
export const refresh = payload => ({
    type:ON_REFRESH,
    payload
});

export const searchStart = () => ({
    type:ON_SEARCH_START
});

export const searchSuccess = payload => ({
    type:ON_SEARCH_SUCCESS,
    payload
});
