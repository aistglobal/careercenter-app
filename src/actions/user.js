import { NavigationActions } from 'react-navigation';

export const LOG_OUT = 'LOG_OUT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const SET_USER_SUCCESS = 'SET_USER_SUCCESS';
export const AUTH_CHECK = 'AUTH_CHECK';
export const REFRESH_TOKEN = 'REFRESH_TOKEN';
export const SET_USER_RESUMES = 'SET_USER_RESUMES';
export const ADD_DEVICE_TOKEN_SUCCESS = 'ADD_DEVICE_TOKEN_SUCCESS';
export const REMOVE_DEVICE_TOKEN_SUCCESS = 'REMOVE_DEVICE_TOKEN_SUCCESS';

export const logOut = () => ({
  type: LOG_OUT,
});
export const checkAuth = () => ({
  type: AUTH_CHECK,
});
export const loginSuccess = payload => ({
  type:LOGIN_SUCCESS,
  payload
})

export const setUser = payload => ({
  type:SET_USER_SUCCESS,
  payload
})

export const refreshToken = payload => {
  return {
    type: REFRESH_TOKEN,
    payload,
  };
};
export const setUserResumes = (payload:Object) => {
  return {
    type: SET_USER_RESUMES,
    payload,
  };
};
// export const refreshToken = () => ({
//   type: REFRESH_TOKEN,
// });

export const logout = () => dispatch => {
  dispatch(logOut());
  dispatch(NavigationActions.navigate({ routeName: 'Login' }));
};

export const addDevice = payload => ({
  type:ADD_DEVICE_TOKEN_SUCCESS,
  payload
});

export const removeDevice = payload => ({
  type:REMOVE_DEVICE_TOKEN_SUCCESS,
  payload
});
