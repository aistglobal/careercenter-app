export const SET_RESUME_SUCCESS = 'SET_RESUME_SUCCESS';
export const REMOVE_RESUME_SUCCESS = 'REMOVE_RESUME_SUCCESS';



export const addResume = payload => ({
    type:SET_RESUME_SUCCESS,
    payload
});

export const removeResume = payload => ({
    type:REMOVE_RESUME_SUCCESS,
    payload
})
