import HTTP from 'axios'
import {logOut, refreshToken} from "../actions/user";
import {NavigationActions} from "react-navigation";
// export const baseUrl = 'https://careercenter.aist.fun';
export const baseUrl = 'https://career.aist.fun/';
export const makeUrl = (lang) => {
    // const baseUrl = 'http://careercenter.loc/api';
    // const baseUrl = 'https://careercenter.aist.fun/api';
    const baseUrl = 'https://career.aist.fun/api';

    const apiVersion = 'v1';
    return `${baseUrl}/${apiVersion}/${lang}`
};
const configurations = {
    baseURL: makeUrl('en'),
    'Content-Type':'application/json',
    'Accept':'application/json'
};

const customHTTP = HTTP.create(configurations)

/**
 * class RefreshToken
 */
export class RefreshToken {
    /**
     *
     * @param  store
     *
     */
    constructor(store:Required) {
        this.isAlreadyFetchingAccessToken = false;
        this.subscribers = [];
        this.store = store;
        const self = this;

        customHTTP.interceptors.response.use(
            function (response) {
                // If the request succeeds, we don't have to do anything and just return the response
                return response
            },
            function (error) {
                const errorResponse = error.response
                if (self.isTokenExpiredError(errorResponse)) {
                    return self.resetTokenAndReattemptRequest(error)
                }
                // If the error is due to other reasons, we just throw it back to axios
                return Promise.reject(error)
            },
        )
    }

    /**
     * function changeLanguage
     * @return void
     */
   static  changeLanguage(language) {

        customHTTP.defaults['baseURL'] = makeUrl(language);
    };
    /**
     *
     * @param errorResponse
     * @return {boolean}
     */
    isTokenExpiredError = (errorResponse) => {
        switch (errorResponse.status) {
            case 422:
            return  false
            case 401:
                this.store.dispatch(logOut)
                this.store.dispatch(NavigationActions.navigate({ routeName: 'Login' }))
                return  false

        }
        return true
        // Your own logic to determine if the error is due to JWT token expired returns a boolean value
    }

    /**
     *
     * @param error
     * @return {Promise<Promise<R>|Promise<Promise<never>|Promise<R>|undefined>>}
     */
    resetTokenAndReattemptRequest = async (error) => {
        try {
            const {response: errorResponse} = error;
            /* Proceed to the token refresh procedure
            We create a new Promise that will retry the request,
            clone all the request configuration from the failed
            request in the error object. */
            const retryOriginalRequest = new Promise(resolve => {
                /* We need to add the request retry to the queue
                since there another request that already attempt to
                refresh the token */
                this.addSubscriber(access_token => {
                    errorResponse.config.headers.Authorization = 'Bearer ' + access_token;
                    resolve(HTTP(errorResponse.config));
                });
            });
            if (!this.isAlreadyFetchingAccessToken) {
                this.isAlreadyFetchingAccessToken = true;
                const response = await HTTP.get(`${makeUrl('en')}/auth/refresh`,
                    {headers: {Authorization: `Bearer ${this.store.getState().user.token}`}});
                if (!response.data) {
                    return Promise.reject(error);
                }
                const newToken = response.headers.authorization;
                // save the newly refreshed token for other requests to use
                this.isAlreadyFetchingAccessToken = false;
                this.onAccessTokenFetched(newToken);
            }
            return retryOriginalRequest;
        } catch (err) {
            console.log('Erroorr')
            return Promise.reject(err);
        }
    }
    /**
     *
     * @param access_token
     */
    onAccessTokenFetched = (access_token) => {
        // When the refresh is successful, we start retrying the requests one by one and empty the queue
        this.store.dispatch(refreshToken(access_token))
        this.subscribers.forEach(callback => callback(access_token));
        this.subscribers = [];
    }
    /**
     *
     * @param callback
     */
    addSubscriber = (callback) => {
        this.subscribers.push(callback);
    }

}

export default customHTTP
