import HTTP from '../utils/HTTP'
import {addDevice, setUser} from "../actions/user";
import * as InfinityLoad from "../actions/InfinityLoad";

export const getJobs = params => async dispatch => {
    try {
        dispatch(InfinityLoad.fetchStart())
        if (params.hasOwnProperty('search')) {
            dispatch(InfinityLoad.searchStart())
        }

        const {data: {data: {jobs, resumes}}} = await HTTP.get('/jobs', {params})
        if (params.hasOwnProperty('search')) {
            dispatch(InfinityLoad.searchSuccess({data: jobs}))
        } else {
            dispatch(InfinityLoad.fetchSuccess({data: jobs}))

        }
        // dispatch(jobSuccess(jobs));
        // dispatch(setUserResumes({resumes}));
        return jobs
    } catch (e) {
        return await Promise.reject(e)
    }

};
export const getJobsHistory = params => async dispatch => {
    try {
        dispatch(InfinityLoad.fetchStart())
        const {data: {data}} = await HTTP.get('/history', {params})
        const history =  data.map((item,index) => {
            return {
                acceptStatus: item.status,
                ...item.job,
                ...item.job.data,
                key:Math.random(1)
            }
        })

            dispatch(InfinityLoad.fetchSuccess({data: history}))

        // dispatch(jobSuccess(jobs));
        // dispatch(setUserResumes({resumes}));
        return history
    } catch (e) {
        return await Promise.reject(e)
    }

}
export const getBookmarks = params => async dispatch => {
    try {
        dispatch(InfinityLoad.fetchStart())
        const {data: {data}} = await HTTP.get('/bookmarks', {params})
        dispatch(InfinityLoad.fetchSuccess({data}))
        return data
    } catch (e) {
        return await Promise.reject(e)
    }

}

export const getJob = slug => HTTP.get(`job/${slug}`);
export const getBlogs = () => HTTP.get(`blog`);
export const getCountries = () => HTTP.get(`countries`);
export const getBlog = slug => HTTP.get(`blog/${slug}`);
export const getTypes = () => HTTP.get(`types`);
export const applyJob = data => HTTP.post(`apply`, data);
export const getCategories = () => HTTP.get(`categories`);

export const bookmarkAdd = job_id => HTTP.post(`bookmark/add`, {job_id});
export const bookmarkRemove = job_id => HTTP.post(`bookmark/remove`, {job_id});

export const addImage = data => HTTP.post(`image`, data);
export const contact = data => HTTP.post(`contact`, data);
export const getNotifications = user_id => HTTP.get(`/notification/get/${user_id}`);
export const addDeviceToken = data => async dispatch => {
    try {
        await HTTP.post(`notification/create`, {token: data.token, device_type: data.os})
        dispatch(addDevice(data))
    } catch (e) {
        console.log(e);
    }

};
export const removeDeviceToken = data => async dispatch => {
    try {

        await HTTP.post(`notification/delete`)

    } catch (e) {
        console.log(e);
    }
};

/**
 *
 * @returns {Promise<AxiosResponse<T>>}
 */
export const updateUser = (data, userId) => async dispatch => {
    try {
        await HTTP.patch(`/user_details/${userId}`, data);
        // dispatch(setUser(data));
        return data;
    } catch (e) {
        return await Promise.reject(e)
    }
}
