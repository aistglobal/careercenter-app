import store from 'react-native-simple-store';
import HTTP from '../utils/HTTP'
import {loginSuccess, setUser} from "../actions/user";
const prefix = 'auth';
/**
 *
 * @param credentials
 * @returns {Promise<unknown|AxiosResponse<T>>}
 */
export const login =  credentials => async dispatch => {
    try{
      const res = await HTTP.post(`/${prefix}/login`,credentials);
      const { authorization : token} = res.headers;
        dispatch(loginSuccess({token}))
        return res;
    }catch (e) {

        throw e.response.data.errors || {login:true};
    }

}
/**
 *
 * @param credentials
 * @returns {Promise<unknown|AxiosResponse<T>>}
 */
export const register =  credentials => async dispatch => {
    try{
        const res = await HTTP.post(`/${prefix}/register`,credentials);
        console.log(res);
        const { authorization : token} = res.headers;
        dispatch(loginSuccess({token}))
        return res;
    }catch (e) {
        throw e.response.data.errors;
    }

}
/**
 *
 * @returns {Promise<AxiosResponse<T>>}
 */
export const getUser = () => async dispatch => {
    try {
        const {data:{data}} = await HTTP.get(`/${prefix}/user`);
        dispatch(setUser(data[0]))
        return data;
    }catch (e) {
        return  await Promise.reject(e)
    }
}
/**
 *
 * @param data
 * @return {Promise<void>}
 */
export const forgetPassword = async data => {
    try {
      await HTTP.post(`/${prefix}/reset/token`, data);
    } catch (e) {
        throw e.response.data.errors;
    }
}
/**
 *
 * @return {Promise<void>}
 * @param reset_token
 */
export const resetToken = async reset_token => {
    try {
     return  await HTTP.get(`/${prefix}/reset/password/${reset_token}`);
    } catch (e) {
        throw e.response.data.errors;
    }
}
/**
 *
 * @return {Promise<void>}
 * @param data
 */
export const resetPassword = async data => {
    try {
     return  await HTTP.post(`/${prefix}/reset/password`,data);
    } catch (e) {
        throw e.response.data.errors;
    }
}
