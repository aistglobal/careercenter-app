import {REMOVE_RESUME_SUCCESS, SET_RESUME_SUCCESS} from "../actions/resumes";

const initialState = {};
export default function resumes(state = initialState, {type, payload = null}) {
    switch (type) {
        case SET_RESUME_SUCCESS:
            return {state, ...payload};
        case REMOVE_RESUME_SUCCESS:
            return {state, ...payload};
        default:
            return state;
    }

}
