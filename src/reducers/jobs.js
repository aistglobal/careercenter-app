import {JOB_SUCCESS} from "../actions/job";
import {LOG_OUT} from "../actions/user";


const initialState = [];

export default function jobs(state = initialState, { type, payload = null }) {
    switch (type) {
        case JOB_SUCCESS:
           return  payload ;
        case LOG_OUT :
            return initialState;
        default:
            return state;
    }
}
