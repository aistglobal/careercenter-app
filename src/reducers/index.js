import { combineReducers } from 'redux';


import user from './user';
import nav from './navigator';
import bookmarks from './bookmarks';
import jobs from './jobs';
import i18n from './i18n';
import resumes from './resumes';
import infinityLoad from './InfinityLoad'

const rootReducer = combineReducers({
  user,
  nav,
  bookmarks,
  jobs,
  i18n,
  resumes,
  infinityLoad
});
export default rootReducer;
