import {CHANGE_LANGUAGE} from '../actions/i18n'

const initialState = {language:'en'};

export default function i18n(state = initialState, { type, payload = null }) {
    switch (type) {
        case CHANGE_LANGUAGE:
            return  {...state,...payload} ;

        default:
            return state;
    }
}
