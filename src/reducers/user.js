import {
  LOG_OUT,
  AUTH_CHECK,
  REFRESH_TOKEN,
  LOGIN_SUCCESS,
  SET_USER_SUCCESS,
  SET_USER_RESUMES,
  ADD_DEVICE_TOKEN_SUCCESS
} from '../actions/user';
import HTTP from '../utils/HTTP';

const initialState = { token: '', isAuthenticated: false, first_name:'',last_name:'' };

export default function user(state = initialState, { type, payload = null }) {
  switch (type) {
    case LOGIN_SUCCESS:
      return authUser(state, payload);
    case AUTH_CHECK:
      return checkAuth(state);
    case REFRESH_TOKEN:
      return refreshToken(state, payload);
    case SET_USER_SUCCESS:

      return { ...state, ...payload };
      case SET_USER_RESUMES:
      return { ...state, ...payload };
      case ADD_DEVICE_TOKEN_SUCCESS:
      return { ...state, device:payload.device };
      case LOG_OUT:
      return { ...state, ...initialState };
    default:
      return state;
  }
}
function refreshToken(state, token) {

  HTTP.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  return {
    ...state,
    token,
    isAuthenticated: true,
  };
}
function authUser(state, payload) {
  HTTP.defaults.headers.common['Authorization'] = `Bearer ${payload.token}`;
  return {
    ...state,
    token: payload.token,
    isAuthenticated: true,
  };
}
function checkAuth(state) {

  if (state.isAuthenticated) {
    HTTP.defaults.headers.common['Authorization'] = `Bearer ${state.token}`;
    return state;
  }
  return {
    ...state,
    token: '',
    isAuthenticated: false,
  };
}
