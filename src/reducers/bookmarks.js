import {REMOVE_BOOKMARK, SET_BOOKMARK, SET_INITIAL_BOOKMARK} from "../actions/bookmarks";
import {LOG_OUT} from "../actions/user";


const initialState = { };

export default function bookmarks(state = initialState, { type, payload = null }) {
    switch (type) {
        case SET_BOOKMARK:
            return { ...state, ...payload };
        case SET_INITIAL_BOOKMARK:
            return { ...state, ...payload };
        case REMOVE_BOOKMARK:
            const stateClone = Object.assign({},state);
            delete stateClone[payload]
            return stateClone;
        case LOG_OUT :
            return initialState;
        default:
            return state;
    }
}
