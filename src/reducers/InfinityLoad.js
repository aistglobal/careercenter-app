import {
    ON_FETCH_FAIL,
    ON_FETCH_START,
    ON_FETCH_SUCCESS,
    ON_LOAD_MORE,
    ON_REFRESH,
    ON_SEARCH_START, ON_SEARCH_SUCCESS
} from "../actions/InfinityLoad";

const initialState = {
    data: [],
    page: 0,
    loading: true,
    loadingMore: false,
    filtering: false,
    refreshing: false,
    error: null
}
export default function infinityLoad(state = initialState, {type, payload = null}) {
    switch (type) {
        case ON_FETCH_START:
            return state;
        case ON_FETCH_SUCCESS:
            return {...state, loading: false, loadingMore: false, refreshing: false, data: state.page === 0 ?  payload.data : [...state.data,...payload.data]};
        case ON_FETCH_FAIL:
            return initialState;
        case ON_LOAD_MORE:
            return {...state, ...payload};
        case ON_REFRESH:
            return {...state, ...payload}
        case ON_SEARCH_START:
            return initialState;
        case ON_SEARCH_SUCCESS:
            return {...initialState,data:payload.data,loading: false, loadingMore: false, refreshing: false}
        default:
            return state;
    }
}
