import AsyncStorage  from '@react-native-community/async-storage';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers/index';
import { middleware } from '../navigator';
const middlewares = [thunk,middleware];

if (__DEV__ && false) {
  middlewares.push(createLogger({}));
}
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist:['infinityLoad']
};
const compose = composeWithDevTools({ realtime: true, fport: 8097 });
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default function configureStore(onCompletion) {
  const enhancer = compose(applyMiddleware(...middlewares));

  const store = createStore(persistedReducer, enhancer);

  const persistor = persistStore(store, [persistConfig, onCompletion(store)]);
  return { store, persistor };
}
