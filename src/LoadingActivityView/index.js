import React from "react";
import {Dimensions, SafeAreaView, View} from 'react-native'
import {ActivityIndicator} from "react-native-paper";
import {translate} from "../utils/translations";


 const LoadingActivityView  =  props => {
    if (!props.visible) {
        return null
    }
    return (
        <SafeAreaView style={{position:'absolute',backgroundColor:'rgba(126,126,126,0.35)',...Dimensions.get("window"),zIndex:10,justifyContent:'center'}}>
            <ActivityIndicator size={"large"} color={'#08a29f'} />
        </SafeAreaView>
    )
}

export default LoadingActivityView
