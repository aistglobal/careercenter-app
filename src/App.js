import React, {Component} from 'react';
import {AppNavigator as Navigator} from './navigator'
import CustomStatusBar from "./components/StatusBar";
import {Alert, View} from "react-native";
import LoadingScreen from './components/LoadingScreen';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import configureStore from './store/configure-store';
import {RefreshToken} from './utils/HTTP'
import {addDevice, checkAuth} from "./actions/user";
import * as RNLocalize from "react-native-localize";
import {setI18nConfig} from "./utils/translations";
import {changeLanguage} from "./actions/i18n";
import NotifService from "./services/NotificationService";
import {Root} from 'popup-ui'

console.disableYellowBox = true;


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            configureStore: configureStore(() => {
            }),
        };

        setI18nConfig();
    }

    componentDidMount() {

        const {store} = this.state.configureStore;
        new RefreshToken(store)
        new NotifService(this.onRegister.bind(this), this.onNotif.bind(this))
        RNLocalize.addEventListener("change", this.handleLocalizationChange);
    }

    componentWillUnmount() {
        RNLocalize.removeEventListener("change", this.handleLocalizationChange);

    }

    handleLocalizationChange = () => {
        setI18nConfig();
        this.forceUpdate();
    };
    changeLanguage = language => {
        const {store} = this.state.configureStore;
        RefreshToken.changeLanguage(language)
        store.dispatch(changeLanguage({language}))
        this.setInitialLanguage(language)
    };
    setInitialLanguage = language => {
        setI18nConfig(language);
        this.forceUpdate();
    }

    onRegister(device) {
        // Alert.alert("Registered !", JSON.stringify(token));
        const {store} = this.state.configureStore;
        store.dispatch(addDevice({device}))
        // this.setState({ registerToken: token.token, gcmRegistered: true });
    }

    onNotif(notif) {
        console.log(notif);
        // Alert.alert(notif.title, notif.message);
    }

    handlePerm(perms) {
        Alert.alert("Permissions", JSON.stringify(perms));
    }

    render() {
        const {store, persistor} = this.state.configureStore;
        return (
            <Root>
                <Provider store={store}>
                    <PersistGate
                        loading={false}
                        persistor={persistor}
                        onBeforeLift={() => {
                            // store.dispatch(refreshToken(''));
                            this.setInitialLanguage(store.getState().i18n.language)
                            store.dispatch(checkAuth())
                        }}
                    >
                        <View style={{flex: 1, backgroundColor: '#ecf0f1'}}>
                            <CustomStatusBar backgroundColor='#005E9D'
                                             networkActivityIndicatorVisible={this.props.networkActivity}
                                             showHideTransition={'slide'}
                                             translucent={true}
                                             animated={true}
                                             barStyle={'light-content'}
                                             hidden={false}
                                             mainProps={this.props}/>
                            <Navigator renderLoadingExperimental={() => <LoadingScreen/>}
                                       uriPrefix="careercenter://"
                                       screenProps={{
                                           changeLanguage: this.changeLanguage,
                                           lang: store.getState().i18n.language
                                       }}
                            />
                        </View>
                    </PersistGate>
                </Provider>
            </Root>
        )
    }
}


export default App;
