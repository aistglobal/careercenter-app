import React, {useState} from "react";
import {Button, SafeAreaView, Text, View} from "react-native";
import StatusBar from "../components/StatusBar";


const Layout = (props) => {
    console.log(props)
    return(
    <View style={{flex:1}}>
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        {props.children}
    </SafeAreaView>
    </View>
)
}
export default  Layout
