import React, {useEffect, useState} from "react";
import {
    BackHandler,
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {ratio} from "../utils/Styles";
import Input from "../components/Input";
import ButtonGradient from "../components/ButtonGradient";
import * as auth from '../services/auth'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {translate} from "../utils/translations";
import LoadingActivityView from "../LoadingActivityView";
import {addDeviceToken} from "../services";
import ButtonGradientBlue from "../components/ButtonGradient/Blue";
import {resetPassword} from "../services/auth";

const ResetPasswordScreen = (props) => {
    const [credentials, setCredentials] = useState({email: ''})
    const [errors, setErrors] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    const resetPass = async () => {
        setIsLoading(true)
        try {
            await resetPassword({...credentials,user_id:props.navigation.getParam('userId')})
            setErrors({})
            props.navigation.navigate('Login')
            setIsLoading(false)
        } catch (e) {
            setIsLoading(false)
            setErrors(e)
        }

    }
    return (
        <ImageBackground source={require('../assets/img/bg.png')} style={styles.container} resizeMode='cover'>
            <LoadingActivityView visible={isLoading}/>
            <KeyboardAvoidingView

                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{flex: 1}}
                enabled={false}
            >
                <SafeAreaView style={styles.container}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.inner}>
                            <Text style={styles.headerTitle}>
                                {translate('reset-password')}
                            </Text>
                            <Input
                                placeholder={translate("new-password")}
                                value={credentials.login}
                                onChange={password => setCredentials({...credentials, password})}
                                error={errors.password}
                                secureTextEntry
                            />
                            <Input
                                placeholder={translate("confirm-password")}
                                value={credentials.login}
                                onChange={password_confirmation => setCredentials({...credentials, password_confirmation})}
                                error={errors.password_confirmation}
                                secureTextEntry
                            />
                            <ButtonGradient text={translate('change-password')} onPress={resetPass}
                                            style={{
                                                width: 200,
                                                marginBottom: 20 * ratio,
                                                marginTop: 40 * ratio,
                                                marginLeft: 'auto',
                                                marginRight: 'auto'
                                            }}/>

                            <View style={{flex: 1}}/>

                        </View>

                    </TouchableWithoutFeedback>
                </SafeAreaView>
            </KeyboardAvoidingView>

            <Image source={require('../assets/img/bg2.png')} style={[styles.imageBg, {bottom: 100, left: 0}]}/>
            <Image source={require('../assets/img/lockbig.png')} style={[styles.imageBg, {bottom: -5, right: -20}]}/>
        </ImageBackground>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: "flex-end",

    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    btnContainer: {
        marginTop: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBg: {
        position: 'absolute'
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 29 * ratio,
        marginVertical: 40 * ratio,
    }
});


export default connect(state => state, dispatch => bindActionCreators({
    login: auth.login,
    addDeviceToken
}, dispatch))(React.memo(ResetPasswordScreen))
