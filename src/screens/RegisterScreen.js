import React, {Fragment, useEffect, useState} from "react";
import {
    Dimensions,
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    View,
    BackHandler, TouchableOpacity, Linking
} from "react-native";
import {isPortrait, isTablet, ratio} from "../utils/Styles";
import Input from "../components/Input";
import ButtonGradient from "../components/ButtonGradient";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {register} from '../services/auth'
import {translate} from "../utils/translations";
import LoadingActivityView from "../LoadingActivityView";
import {getCountries} from "../services";
import Select from "../components/Input/Select";
import GenderObject from '../../resources/Gender'
import DatePicker from "../components/Input/DatePicker";
import { CheckBox } from 'react-native-elements';
import {baseUrl} from "../utils/HTTP";

const RegisterScreen = (props) => {
    const [data, setData] = useState({
        phone_type:'primary',
        address:[],
        city:[],
        zip:[],
    })
    const [errors, setErrors] = useState({})
    const [countries, setCountries] = useState([])
    const [isAdvanced, setIsAdvanced] = useState(false)
    const [orientation, setOrientation] = useState(isPortrait() ? 'portrait' : 'landscape');
    const [isLoading, setIsLoading] = useState(false)
    const [terms, setTerms] = useState(false)
    useEffect(() => {
        (async ()=>{
         const {data:{data}} = await getCountries()
            setCountries(data)
        })()

    },[])
    useEffect(() => {
        Dimensions.addEventListener('change', () => {
            setOrientation(
                isPortrait() ? 'portrait' : 'landscape'
            );
        });
        return () => {
            Dimensions.removeEventListener('change')
        }
    }, [])
    console.log(orientation)
    const handleSubmit = async () => {
        setIsLoading(true)
        try {
            await props.register(data)
            props.navigation.navigate('Home')
            setIsLoading(false)
        } catch (e) {
            setIsLoading(false)
            setErrors(e)
        }

    }
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    const width = orientation === 'landscape' && !isTablet ? '48%' : null
    return (
        <ImageBackground source={require('../assets/img/bg.png')} style={styles.container} resizeMode='cover' imageStyle={{transform:[{rotate:'180deg'},{translateY:150}]}}>
            <LoadingActivityView visible={isLoading}/>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{flex: 1}}
            >
                <ScrollView style={styles.container}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.inner}>

                            <Text style={styles.headerTitle}>
                                {translate('register')}
                            </Text>
                            <View
                                style={[orientation === 'landscape' && !isTablet ? styles.twoRow : null, {
                                    zIndex: 13,
                                }]}>

                                <Input
                                    placeholder={translate("first_name")}
                                    value={data.first_name}
                                    onChange={first_name => setData({...data, first_name})}
                                    width={width}
                                    error={errors.first_name}

                                />
                                <Input
                                    placeholder={translate("last_name")}
                                    value={data.last_name}
                                    onChange={last_name => setData({...data, last_name})}
                                    width={width}
                                    error={errors.last_name}
                                />
                                <Input
                                    placeholder={translate("username")}
                                    value={data.username}
                                    onChange={username => setData({...data, username})}
                                    width={width}
                                    error={errors.username}
                                />
                                <Input
                                    placeholder={translate("email")}
                                    value={data.email}
                                    onChange={email => setData({...data, email})}
                                    width={width}
                                    error={errors.email}
                                />
                                <Input
                                    placeholder={translate("phone")}
                                    value={data.phone}
                                    onChange={phone => setData({...data, phone})}
                                    width={width}
                                    error={errors.phone}
                                />
                                <Input
                                    placeholder={translate('password')}
                                    value={data.password}
                                    onChange={password => setData({...data, password})}
                                    labelActiveBackground="#daf2ef"
                                    width={width}
                                    error={errors.password}
                                    secureTextEntry
                                />
                                <Input
                                    placeholder={translate('password_confirmation')}
                                    value={data.password_confirmation}
                                    onChange={password_confirmation => setData({...data, password_confirmation})}
                                    labelActiveBackground="#9FDED7"
                                    width={width}
                                    error={errors.password && errors.password[0] == 'The password confirmation does not match.'}
                                    secureTextEntry
                                />
                                {isAdvanced && <Fragment>
                                    <Select objectRecompose={{key:'value',label:'label'}}
                                            value={data.title}
                                            items={GenderObject}
                                            placeholder={translate('gander')}
                                            onValueChange={title => setData({...data, title})}
                                    />
                                    <Select
                                        items={countries}
                                        objectRecompose={{key:'id',label:'name'}}
                                        onValueChange={residence_country_id => setData({...data, residence_country_id})}
                                        value={data.residence_country_id}
                                        placeholder={translate('country_of_residence')}
                                        width={width}
                                        error={errors.residence_country_id}
                                    />
                                    <Select
                                        items={countries}
                                        objectRecompose={{key:'id',label:'name'}}
                                        onValueChange={citizenship_id => setData({...data, citizenship_id})}
                                        value={data.citizenship_id}
                                        placeholder={translate('citizenship')}
                                        width={width}
                                        error={errors.citizenship_id}
                                    />
                                    <Select
                                        items={countries}
                                        objectRecompose={{key:'id',label:'name'}}
                                        onValueChange={country => setData({...data, country})}
                                        value={data.country}
                                        placeholder={translate('country')}
                                        width={width}
                                        error={errors.country}
                                    />
                                    <Input
                                        placeholder={translate("address")}
                                        value={data.address[0]}
                                        onChange={address => setData({...data, address:[address]})}
                                        width={width}
                                        error={errors.address}
                                    />
                                    <Input
                                        placeholder={translate("city")}
                                        value={data.city[0]}
                                        onChange={city => setData({...data, city:[city]})}
                                        width={width}
                                        error={errors.city}
                                    />
                                    <Input
                                        placeholder={translate("zip")}
                                        value={data.zip[0]}
                                        onChange={zip => setData({...data, zip:[zip]})}
                                        width={width}
                                        error={errors.zip}
                                    />

                                    <Input
                                        placeholder={translate("middle_name")}
                                        value={data.middle_name}
                                        onChange={middle_name => setData({...data, middle_name})}
                                        width={width}
                                        error={errors.middle_name}
                                    />
                                    <DatePicker
                                        placeholder={translate("date_of_birth")}
                                        value={data.birthday}
                                        onChange={birthday => setData({...data, birthday})}
                                    />
                                    <Input
                                        placeholder={translate("website")}
                                        value={data.website}
                                        onChange={website => setData({...data, website})}
                                        width={width}
                                        error={errors.website}
                                    />

                                </Fragment>}
                            </View>
                            <Image source={require('../assets/img/register_image.png')}
                                   style={[styles.imageBg, {
                                       bottom: 0,
                                       right: -30 * ratio,
                                       zIndex: 0,
                                       height: 120 * ratio
                                   }]} resizeMode={"contain"}/>

                            <Image source={require('../assets/img/bg2.png')}
                                   style={[styles.imageBg, {bottom: 100, left: 0, zIndex: -1}]}/>
                            <View style={styles.btnContainer}>
                                <TouchableOpacity onPress={() => setIsAdvanced(!isAdvanced)} style={{marginBottom:20 * ratio}}>
                                    <Text style={{color:'#0fa48e',textDecorationLine:'underline'}}>
                                        {translate('advanced_registration')}
                                    </Text>
                                </TouchableOpacity>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        width: Dimensions.get('screen').width - 60,
                                        height: 50,
                                    }}
                                >
                                    <CheckBox
                                        containerStyle={{
                                            backgroundColor: 'transparent',
                                            borderWidth: 0,
                                            margin: 0,
                                            marginTop: 20,
                                            padding: 0,
                                            height: 30,
                                            width: 30,
                                        }}
                                        iconType="ionicon"
                                        checkedIcon="ios-checkbox-outline"
                                        uncheckedIcon="ios-square-outline"
                                        checkedColor="grey"
                                        checked={terms}
                                        onPress={() =>
                                            setTerms(!terms)
                                        }
                                    />
                                    <Text
                                        style={[
                                            styles.buttonContainerText,
                                            { height: 100, maxWidth: 250 },
                                        ]}
                                    >
                                        {translate('i_agree_to_the')}{' '}
                                        <Text
                                            onPress={() =>
                                                Linking.openURL(`${baseUrl}/terms-and-conditions`)
                                            }
                                            style={{ textDecorationLine: 'underline',color: '#0093d6' }}
                                        >
                                            {translate('terms_of_service')}
                                        </Text>
                                    </Text>
                                    {/*</Text>*/}
                                </View>
                                <ButtonGradient onPress={handleSubmit} text={translate('register')}/>
                            </View>
                            <View style={{flex: 1}}/>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </KeyboardAvoidingView>

        </ImageBackground>
    )

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: "flex-end",

    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    btnContainer: {
        marginTop: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBg: {
        position: 'absolute',
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 29 * ratio,
        marginVertical: 40 * ratio,
    },
    twoRow: {
        // flex:1,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    buttonContainerText:{
        textAlign: 'center',
        marginTop: 20,
    }
});


export default connect(state => state, dispatch => bindActionCreators({register}, dispatch))(React.memo(RegisterScreen))
