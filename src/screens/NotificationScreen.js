import React, {useEffect, useState} from "react";
import {BackHandler, ScrollView, Text} from "react-native";
import Container from "../components/Container";
import {getNotifications} from "../services";
import {connect} from "react-redux";
import JobItem from "../components/Job/JobItem";
import {baseUrl} from "../utils/HTTP";
import moment from "moment";


const NotificationScreen = (props) => {
    const [notifications,setNotification]  = useState([])
    useEffect(() => {
        getNotifications(props.user.id).then(({data:{data}}) => setNotification(data))
    },[])
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return(
        <Container>
            <ScrollView style={{width:'100%'}}>
          {notifications.map(item => {
             return (<JobItem
                  image={item.logo && item.logo.length > 0 ? {uri: `${baseUrl}storage/logos/${item.logo}`} : null}
                  key={item.jobId}
                  title={item.data.title}
                  subTitle={item.data.message}
                  date={moment(item.updated_at).format('DD MMM YYYY')}
                  hideBookmark
                  acceptStatus={item.acceptStatus || false}

              />)
          })}
            </ScrollView>
        </Container>
)}

export default connect(state => state)(React.memo(NotificationScreen))
