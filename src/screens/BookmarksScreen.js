import React, {Fragment, useEffect, useState} from "react";
import {BackHandler, ScrollView, Text} from "react-native";
import Container from "../components/Container";
import JobItem from "../components/Job/JobItem";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {removeBookmark, setBookmark, setInitialBookmarks} from "../actions/bookmarks";
import JobItems from "../components/Job/JobItems";
import {getBookmarks} from "../services";
import {translate} from "../utils/translations";


const BookmarksScreen = (props) =>{
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return(

        <Container>
            <ScrollView style={{flex: 1, width: "100%", paddingTop: 20}}>
                <JobItems bookmarkedJobs={Object.keys(props.bookmarks).map(item => props.bookmarks[item])} navigation={props.navigation} isBookmarks noResultMessage={translate("bookmarks_not_found")}/>
            </ScrollView>
        </Container>
    )}

export default connect(state => state)(React.memo(BookmarksScreen))
