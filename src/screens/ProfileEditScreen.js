import React, {Fragment, useEffect, useState} from "react";
import {
    BackHandler,
    Dimensions,
    ImageBackground,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View
} from "react-native";
import Container from "../components/Container";
import {isPortrait, ratio} from "../utils/Styles";
import Input from "../components/Input";
import ButtonGradient from "../components/ButtonGradient";
import ButtonGradientBlue from "../components/ButtonGradient/Blue";
import CustomIcon from "../utils/CustomIcon";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getCountries, updateUser} from "../services";
import {translate} from "../utils/translations";
import DatePicker from "../components/Input/DatePicker";
import Select from "../components/Input/Select";
import GenderObject from "../../resources/Gender";
import { Popup } from 'popup-ui'
import LoadingActivityView from "../LoadingActivityView";

const styles = StyleSheet.create({
    list: {
        flex: 1,
        width: '100%',
        padding: 20 * ratio
    },
    listItem: {
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        // justifyContent:'center',
        height: 50 * ratio,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(112,112,112,0.27)'

    },
    listItemText: {
        marginLeft: 20 * ratio,
        fontSize: 14 * ratio,
    },
    inputBordered: {
        borderBottomWidth: 1,
        borderBottomColor: '#707070',
        width: "90%",
        marginTop: 'auto'
    },
    inputGroup: {
        flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', height: 50
    }
})
const ProfileScreen = (props) => {
    const [data, setData] = useState({phone:{},country:[],address:[],title:'',residence_country_id:''})
    const [countries, setCountries] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    const [orientation, setOrientation] = useState(isPortrait() ? 'portrait' : 'landscape');
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    useEffect(() => {
        (async ()=>{
            const {data:{data}} = await getCountries()
            setCountries(data)
        })()

    },[])
    useEffect(() => {
        const userData = Object.assign({}, props.user);
        delete userData.token
        setData({...userData,
            phone:JSON.parse(userData.phone).length > 0 ? [JSON.parse(userData.phone)[0].phone]: [],
            phone_type:JSON.parse(userData.phone).length > 0 ? [JSON.parse(userData.phone)[0].type]: ['primary'],
            residence_country_id:userData.residence_country_id || '',
            address:userData.address.length > 0 ? [userData.address[0].address] : []
        })
        Dimensions.addEventListener('change', () => {
            setOrientation(
                isPortrait() ? 'portrait' : 'landscape'
            );
        });
        return () => {
            Dimensions.removeEventListener('change')
        }
    }, [props.user])
    // const width = orientation === 'landscape' && !DeviceInfo.isTablet() ? '48%' : null
    const width = '100%'
    return (

        <Container>
            <LoadingActivityView visible={isLoading}/>
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff', width: '100%', justifyContent: 'center'}}>
                <ScrollView style={styles.list} contentContainerStyle={{paddingVertical: 80 * ratio}}>
                    <Input
                        placeholder={translate("first_name")}
                        value={data.first_name}
                        onChange={first_name => setData({...data, first_name})}
                        width={width}
                    />
                    <Input
                        placeholder={translate("last_name")}
                        value={data.last_name}
                        onChange={last_name => setData({...data, last_name})}
                        width={width}
                    />
                    <Select objectRecompose={{key:'value',label:'label'}}
                            value={data.title}
                            items={GenderObject}
                            placeholder={translate('gander')}
                            onValueChange={title => setData({...data, title})}
                    />
                    <Input
                        placeholder={translate('phone')}
                        value={data.phone[0]}
                        onChange={phone => setData({...data, phone:[phone]})}
                        width={width}
                    />
                    <DatePicker
                        placeholder={translate("date_of_birth")}
                        value={data.birthday}
                        onChange={birthday => setData({...data, birthday})}
                        width={width}
                    />
                    <Input
                        placeholder={translate('email')}
                        value={data.email}
                        onChange={email => setData({...data, email})}
                        width={width}
                    />

                    <Select
                        items={countries}
                        objectRecompose={{key:'id',label:'name'}}
                        onValueChange={residence_country_id => setData({...data, residence_country_id})}
                        value={data.residence_country_id}
                        placeholder={translate('country')}
                        width={width}
                    />
                    <Input
                        placeholder={translate("address")}
                        value={data.address[0]}
                        onChange={address => setData({...data, address:[address]})}
                        width={width}
                    />
                    <Text>{translate('links')}</Text>
                    <View style={{
                        width: '100%',
                        height: 70 * ratio,
                        justifyContent: 'space-between',
                        paddingVertical: 10 * ratio,
                        marginBottom: 60 * ratio
                    }}>
                        <View style={styles.inputGroup}>
                            <CustomIcon name="fb" size={20} color="#385C8E" style={{marginTop: 'auto'}}/>
                            <TextInput style={styles.inputBordered}/>
                        </View>
                        <View style={styles.inputGroup}>
                            <CustomIcon name="twittter" size={15} color="#03A9F4" style={{marginTop: 'auto'}}/>
                            <TextInput style={styles.inputBordered}/>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <ButtonGradientBlue text={translate("discard")} onPress={() => {
                            setData(props.user);
                            props.navigation.goBack();
                        }}/>
                        <ButtonGradient text={translate("save_changes")} onPress={async () => {
                            try {
                                setIsLoading(true)
                                await props.updateUser(data, data.id);
                                setIsLoading(false)
                                Popup.show({
                                    type: 'Success',
                                    title: translate('profile_update'),
                                    button: false,
                                    textBody: translate('profile_updated_successfully'),
                                    buttontext: translate('ok'),
                                    callback: () => Popup.hide(),
                                })
                                // props.navigation.goBack();
                            } catch (e) {
                                setIsLoading(false)
                                console.log(e);
                            }

                        }}/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </Container>
    )
}

export default connect(state => state, d => bindActionCreators({updateUser}, d))(React.memo(ProfileScreen))
