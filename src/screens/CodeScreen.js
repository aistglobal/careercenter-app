import React, {useEffect, useState} from "react";
import {
    BackHandler,
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text, TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {ratio} from "../utils/Styles";
import ButtonGradient from "../components/ButtonGradient";
import * as auth from '../services/auth'
import {forgetPassword} from '../services/auth'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {translate} from "../utils/translations";
import LoadingActivityView from "../LoadingActivityView";
import {addDeviceToken} from "../services";
import ButtonGradientBlue from "../components/ButtonGradient/Blue";
import CodeInput from 'react-native-confirmation-code-input';
import {resetToken} from "../services/auth";

const CodeScreen = (props) => {
    const [credentials, setCredentials] = useState({email: ''})
    const [errors, setErrors] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    const _onFinishCheckingCode1 = async (token) => {
        setIsLoading(true)
        try{
            const {data:{data}} = await resetToken(token);
            setIsLoading(false)
            props.navigation.navigate('ResetPassword',{userId:data.user_id})
        }catch (e) {
            setIsLoading(false)
            setErrors({error:true})
            setTimeout(() => {
                setErrors({})
            },2000)
        }

    }
    return (
        <ImageBackground source={require('../assets/img/bg.png')} style={styles.container} resizeMode='cover'>
            <LoadingActivityView visible={isLoading}/>
            <KeyboardAvoidingView

                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{flex: 1}}
                enabled={false}
            >
                <SafeAreaView style={styles.container}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.inner}>
                            <Text style={styles.headerTitle}>
                                {translate('verification_code')}
                            </Text>
                            <Text>
                                {translate('verification_message')}
                            </Text>
                            <CodeInput
                                // secureTextEntry
                                activeColor={errors.error ?  'red' :'#066FA6'}
                                inactiveColor={errors.error ? 'red' :'#1FA092'}
                                autoFocus={false}
                                ignoreCase={true}
                                inputPosition='center'
                                size={50}
                                codeLength={6}
                                onFulfill={(isValid) => _onFinishCheckingCode1(isValid)}
                                containerStyle={{marginTop: 30,maxHeight:50}}
                                codeInputStyle={{borderWidth: 1.5}}
                            />
                            <TouchableOpacity onPress={async () => {
                                await forgetPassword({email:props.navigation.getParam('email')})
                            }} style={{marginVertical:70 * ratio,}}><Text style={{textDecorationLine:'underline',color:'#067ECD',textAlign:'center',fontWeight:'bold'}}>{translate('resend_code')}</Text></TouchableOpacity>
                            <ButtonGradientBlue text={translate('cancel')} onPress={() => props.navigation.goBack()}
                                                style={{marginLeft: 'auto', marginRight: 'auto',marginTop: 50 * ratio}}/>
                                        <View style={{flex:1}}/>
                        </View>

                    </TouchableWithoutFeedback>
                </SafeAreaView>
            </KeyboardAvoidingView>

            <Image source={require('../assets/img/CODE_BACKGROUND.png')} style={[styles.imageBg, {bottom: 30, left: 0,zIndex:-1}]}/>
        </ImageBackground>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: "flex-end",

    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    btnContainer: {
        marginTop: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBg: {
        position: 'absolute'
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 29 * ratio,
        marginVertical: 40 * ratio,
    }
});


export default connect(state => state, dispatch => bindActionCreators({
    login: auth.login,
    addDeviceToken
}, dispatch))(React.memo(CodeScreen))
