import React, {useEffect, useState} from "react";
import {
    BackHandler,
    ImageBackground, Keyboard,
    KeyboardAvoidingView,
    Linking, Platform, SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity, TouchableWithoutFeedback,
    View
} from "react-native";
import Container from "../components/Container";
import IonIcon from "react-native-vector-icons/Ionicons";
import {isTablet, ratio} from "../utils/Styles";
import CustomIcon from "../utils/CustomIcon";
import ButtonGradientBlue from "../components/ButtonGradient/Blue";
import Icon from 'react-native-vector-icons/FontAwesome5'
import {contact} from "../services";
import {connect} from "react-redux";
import {Popup} from "popup-ui";
import {translate} from "../utils/translations";

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: '100%',
        padding: 0,
    },
    container: {
        flex: 1,
        width: '100%',
        padding: 10 * ratio,
        marginLeft: 5 * ratio
    },
    title:{
        fontSize:18,
        fontWeight:'bold',
        marginBottom:10
    },
    button:{
        flexDirection:'row',
        padding:10
    },
    buttonText:{
        marginLeft: 10
    },
    input:{
        borderWidth:1,
        borderColor:'#989797',
        flex:1,
        marginVertical:20,
        borderRadius:4,
        maxHeight:200,
        minHeight:100,
        padding:10
    }
})
const ContactUsScreen = (props) => {
    let h = isTablet ? 70 * ratio : 50 * ratio;
    const [message ,setMessage] = useState('')
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
   const dialCall = () => {

        let phoneNumber = '';

        if (Platform.OS === 'android') {
            phoneNumber = 'tel:+37410519062';
        }
        else {
            phoneNumber = `telprompt:+37410519062`;
        }

        Linking.openURL(phoneNumber);
    };
    const submit = async () => {
        const {first_name,last_name,email} = props.user
        if(!message){
            Popup.show({
                type: 'Danger',
                title:translate('error'),
                button: false,
                textBody: translate('message_field_is_required'),
                buttontext: translate('ok'),
                callback: () => Popup.hide(),
            })
        }
        try{
         await contact({
                name:`${first_name} ${last_name}`,
                email,
                text:message
            })
            Popup.show({
                type: 'Success',
                title:translate('success'),
                button: false,
                textBody: translate('your_message_successfully_send'),
                buttontext: translate('ok'),
                callback: () => Popup.hide(),
            })
            setMessage('')
            Keyboard.dismiss()
        }catch (e) {
            setMessage('')
            Keyboard.dismiss()
            console.log(e);

        }
    }
    return (
        <Container style={{justifyContent: 'flex-start'}}>

            <View style={[styles.image, {minHeight: h + 30, maxHeight: h + 30},Platform.OS === "ios" && {zIndex:100}]}>
                <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                                 source={require('../components/Navigation/img/home_gradient.png')}
                                 resizeMode={"cover"}>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
                            <CustomIcon name="line-menu" size={20 * ratio} color='#fff'/>
                        </TouchableOpacity>

                    </View>
                </ImageBackground>
            </View>
            <KeyboardAvoidingView

                behavior={Platform.OS === "ios" ? "position" : null}
                style={{flex: 1}}
                enabled={Platform.OS === "ios"}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <Text style={styles.title}>{translate('contact_us')}</Text>
                <View>
                    <TouchableOpacity style={styles.button} onPress={dialCall}><CustomIcon name="phone-contact" size={22} color="#006DB7"/><Text style={styles.buttonText}>+374 10 519 062</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => Linking.openURL('mailto:mailbox@careerhouse.com') }><IonIcon name="md-mail" size={22} color="#006DB7"/><Text style={styles.buttonText}>mailbox@careerhouse.com</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => Linking.openURL('https://www.careerhouse.com')}><Icon name="globe" size={22} color="#006DB7"/><Text style={styles.buttonText}>www.careerhouse.com</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => Linking.openURL('https://www.google.com/maps/place/25+Abovyan+poxoc,+Yerevan+0001,+%D0%90%D1%80%D0%BC%D0%B5%D0%BD%D0%B8%D1%8F/@40.1859837,44.5189335,17z/data=!3m1!4b1!4m5!3m4!1s0x406abce124637e8d:0x18598723afdfedc7!8m2!3d40.1859837!4d44.5211222')}><Icon name="map-marker-alt" size={22} color="#006DB7"/><Text style={styles.buttonText}>{translate('address__')}</Text></TouchableOpacity>
                </View>
                <TextInput style={styles.input} value={message} multiline placeholder={translate('message')} returnKeyType={"send"} onChangeText={message => setMessage(message)} />
                <ButtonGradientBlue text='Send' onPress={submit} style={{marginLeft:'auto',marginRight:'auto',marginBottom: 20 * ratio,marginTop:"auto"}}/>
            </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </Container>
    )
}

export default connect(state => state)(React.memo(ContactUsScreen))
