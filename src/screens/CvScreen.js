import React, {useEffect, useState} from "react";
import {BackHandler, FlatList, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity} from "react-native";
import Container from "../components/Container";
import {ratio} from "../utils/Styles";
import * as auth from '../services/auth';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {logout} from "../actions/user";
import {translate} from "../utils/translations";
import HTTP, {baseUrl, makeUrl} from '../utils/HTTP'
import ButtonGradient from "../components/ButtonGradient";
import {addResume} from "../actions/resumes";
import Icon from "react-native-vector-icons/Ionicons";
import DocumentPicker from 'react-native-document-picker';
import * as RNFS from 'react-native-fs'
import axios from 'axios'
const styles = StyleSheet.create({
    list: {
        flex: 1,
        width: '100%',
        padding: 20 * ratio
    },
    listItem: {
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        // justifyContent:'center',
        height: 50 * ratio,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(112,112,112,0.27)'

    },
    listItemText: {
        marginLeft: 20 * ratio,
        fontSize: 14 * ratio,
    }
})
const CvScreen = (props) => {
    const [resumes, setResumes] = useState([])
    useEffect(() => {
        HTTP.get('/jobs').then(({data: {data: {resumes}}}) => {
            setResumes(resumes)
        })
    }, [])
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return (
        <Container>
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff', width: '100%', justifyContent: 'center'}}>
                <ScrollView style={styles.list} contentContainerStyle={{paddingVertical: 20 * ratio}}>
                    <Text style={{textAlign: 'center'}}>{translate('cv')}</Text>
                    <FlatList style={{flex: 1}} data={resumes} renderItem={({item, index}) => <TouchableOpacity
                        style={{padding: 12, flexDirection: 'row', justifyContent: 'space-between',borderBottomWidth:1,borderColor:'grey'}}
                        onPress={() => props.addResume(item)}><Text>{translate('resume') + ' ' + (index + 1)}</Text>
                    </TouchableOpacity>}/>
                </ScrollView>
            </SafeAreaView>
        </Container>
    )
}

export default connect(state => state, dispatch =>
    bindActionCreators({
        getUser: auth.getUser,
        logout: logout,
        addResume
    }, dispatch))(React.memo(CvScreen))
