import React, {useEffect, useState} from "react";
import {
    Image,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Linking, BackHandler
} from "react-native";
import Container from "../components/Container";
import Ionicons from "react-native-vector-icons/Ionicons";
import {isTablet, ratio, screenWidth} from "../utils/Styles";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ButtonGradient from "../components/ButtonGradient";
import BlogCard from "../components/BlogCard";
import {getBlog} from "../services";
import {baseUrl} from "../utils/HTTP";
import moment from "moment";
import HTML from 'react-native-render-html';

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: '100%',
        padding: 0,
    },
    container: {
        flex: 1,
        width: '100%',
        padding: 10 * ratio,
        marginLeft: 5 * ratio
    },
    text: {
        fontSize: 16 * ratio,
        color: '#fff',
        textAlign: 'center',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    line: {
        backgroundColor: '#fff',
        height: 3 * ratio,

    },
    imageProfileContainer: {
        width: 101 * ratio,
        height: 81 * ratio,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#DDDDDD',
        marginLeft: 30 * ratio,
        position: 'absolute',
        bottom: -30,
        zIndex: 100
    },
    subtitle: {
        fontWeight: 'bold'
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        minHeight:30
    },
    subTitleMessage:{
        textAlign:'left',
        width:screenWidth / 3,
        maxWidth:screenWidth / 3,
    }
})
const BlogSingleScreen = (props) => {
    const [htmlContent,setHtmlContent] = useState('');
    let h = isTablet ? 70 * ratio : 50 * ratio;
    useEffect(()=> {
        const slug = props.navigation.getParam('slug')
        getBlog(slug).then(({data}) => {
            console.log(data.data.blog.data.content);
            setHtmlContent(data.data.blog.data.content)
            })
    },[])
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return (
        <Container style={{justifyContent: 'flex-start'}}>
            <View style={[styles.image, {minHeight: h + 30, maxHeight: h + 30}]}>
                <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                                 source={require('../components/Navigation/img/home_gradient.png')}
                                 resizeMode={"cover"}>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => props.navigation.goBack()} style={{flexDirection:'row',alignItems:'center'}}>
                            <Ionicons name="ios-arrow-back" size={27 * ratio} color='#fff'/><Text style={{marginLeft:10,color:'#fff'}}>Blog</Text>
                        </TouchableOpacity>

                    </View>
                </ImageBackground>
            </View>
            <ScrollView  style={{flex:1,width:'100%',padding:10 * ratio}} >
                <HTML html={htmlContent} imagesMaxWidth={Dimensions.get('window').width} onLinkPress={(evt, href, htmlAttribs) => {
                   Linking.openURL(href)
                }} />
            </ScrollView>
        </Container>
    )
}

export default React.memo(BlogSingleScreen)
