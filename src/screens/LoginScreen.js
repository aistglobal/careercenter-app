import React, {useEffect, useState} from "react";
import {
    Dimensions,
    Image,
    ImageBackground,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {isPortrait, ratio} from "../utils/Styles";
import Input from "../components/Input";
import ButtonGradient from "../components/ButtonGradient";
import {changeLanguage, setAuthorizationToken} from "../utils/HTTP";
import * as auth from '../services/auth'
import store from 'react-native-simple-store';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {translate} from "../utils/translations";
import LoadingActivityView from "../LoadingActivityView";
import {addDeviceToken} from "../services";
const LoginScreen = (props) => {
    const [credentials,setCredentials] = useState({login:__DEV__? 'vahe.saroyan.web@gmail.com' :'',password:__DEV__? 'qwerty12345' :''})
    const [errors,setErrors] = useState({})
    const [isLoading,setIsLoading] = useState(false)

    const login = async () => {
        setIsLoading(true)
        try{
            await props.login(credentials)
            await props.addDeviceToken(props.user.device);
            props.navigation.navigate('Home')
            setIsLoading(false)
        }catch (e) {
            setIsLoading(false)
            setErrors(e)
        }

    }

    return (
        <ImageBackground source={require('../assets/img/bg.png')} style={styles.container} resizeMode='cover'>
            <LoadingActivityView visible={isLoading}/>
            <KeyboardAvoidingView

                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{flex: 1}}
                enabled={false}
            >
                {/*<TouchableOpacity onPress={()=> {*/}
                {/*        store.save("settings", {*/}
                {/*        language: 'ru'*/}
                {/*    }).then(() => {*/}
                {/*            changeLanguage()*/}
                {/*        })*/}
                {/*}}><Text>changeLang ru</Text></TouchableOpacity>*/}
                {/*<TouchableOpacity onPress={()=> {*/}
                {/*    store.save("settings", {*/}
                {/*        language: 'en'*/}
                {/*    }).then(() => {*/}
                {/*        changeLanguage()*/}
                {/*    })*/}
                {/*}}><Text>changeLang en</Text></TouchableOpacity>*/}

                <SafeAreaView style={styles.container}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.inner}>
                            <Text style={styles.headerTitle}>
                                {translate('login')}
                            </Text>
                            <Input
                                placeholder={translate("username")}
                                value={credentials.login}
                                onChange={login => setCredentials({...credentials,login})}
                                error={errors.login}
                            />
                            <Input
                                placeholder={translate("password")}
                                value={credentials.password}
                                onChange={password => setCredentials({...credentials,password})}
                                secureTextEntry
                                error={errors.password}

                            />
                            <TouchableOpacity style={{marginTop: 10 * ratio, marginBottom: 30 * ratio}} onPress={()=>props.navigation.navigate('Forgot')}>
                                <Text>{translate('forgot_password')}?</Text>
                            </TouchableOpacity>
                            <View style={styles.btnContainer}>
                                <ButtonGradient onPress={login} text={translate('login')}/>
                            </View>
                            <TouchableOpacity style={{marginTop: 30 * ratio}} onPress={()=>props.navigation.navigate('Register')}>
                                <Text style={{color: '#005E9D', textAlign: 'center'}}>{translate('want_to_create_an_account')}?</Text>
                            </TouchableOpacity>
                            <View style={{flex: 1}}/>
                        </View>
                    </TouchableWithoutFeedback>
                </SafeAreaView>
            </KeyboardAvoidingView>

            <Image source={require('../assets/img/bg2.png')} style={[styles.imageBg, {bottom: 100, left: -50}]}/>
        </ImageBackground>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: "flex-end",

    },
    header: {
        fontSize: 36,
        marginBottom: 48,
    },
    btnContainer: {
        marginTop: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageBg: {
        position: 'absolute'
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 29 * ratio,
        marginVertical: 40 * ratio,
    }
});


export default connect(state => state,dispatch => bindActionCreators({login:auth.login,addDeviceToken},dispatch))(React.memo(LoginScreen))
