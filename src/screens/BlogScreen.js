import React, {useEffect, useState} from "react";
import {BackHandler, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Container from "../components/Container";
import Ionicons from "react-native-vector-icons/Ionicons";
import {isTablet, ratio, screenWidth} from "../utils/Styles";

import BlogCard from "../components/BlogCard";
import {getBlogs} from "../services";
import {baseUrl} from "../utils/HTTP";
import moment from "moment";
import BlogSingleScreen from "./BlogSingleScreen";
import CustomIcon from "../utils/CustomIcon";
import {connect} from "react-redux";

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: '100%',
        padding: 0,
    },
    container: {
        flex: 1,
        width: '100%',
        padding: 10 * ratio,
        marginLeft: 5 * ratio
    },
    text: {
        fontSize: 16 * ratio,
        color: '#fff',
        textAlign: 'center',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    line: {
        backgroundColor: '#fff',
        height: 3 * ratio,

    },
    imageProfileContainer: {
        width: 101 * ratio,
        height: 81 * ratio,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#DDDDDD',
        marginLeft: 30 * ratio,
        position: 'absolute',
        bottom: -30,
        zIndex: 100
    },
    subtitle: {
        fontWeight: 'bold'
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        minHeight: 30
    },
    subTitleMessage: {
        textAlign: 'left',
        width: screenWidth / 3,
        maxWidth: screenWidth / 3,
    }
})
const BlogScreen = (props) => {
    const [blogs, setBlogs] = useState([]);
    let h = isTablet ? 70 * ratio : 50 * ratio;
    useEffect(() => {
        getBlogs().then(({data: {data: {blogs}}}) => {
            console.log(blogs)
            setBlogs(blogs)
        })
    }, [props.i18n.language])
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return (
        <Container style={{justifyContent: 'flex-start'}}>
            <View style={[styles.image, {minHeight: h + 30, maxHeight: h + 30}]}>
                <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                                 source={require('../components/Navigation/img/home_gradient.png')}
                                 resizeMode={"cover"}>
                    <View style={styles.container}>
                    <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
                    <CustomIcon name="line-menu" size={20 * ratio} color='#fff'/>
                    </TouchableOpacity>

                    </View>
                </ImageBackground>
            </View>
            <ScrollView style={{flex: 1, width: '100%', padding: 10 * ratio}}>
                {blogs.map(({data, ...item}) => {
                    return <BlogCard title={data.title}
                                     date={moment(item.created_at).format("Y MMM DD")}
                                     image={{uri: `${baseUrl}/storage/${item.image}`}}
                                     onPress={() => {
                                         props.navigation.navigate('BlogSingleScreen', {slug: item.slug})
                                     }}

                    />
                })}
            </ScrollView>
        </Container>
    )
}

export default connect(state => state)(React.memo(BlogScreen))
