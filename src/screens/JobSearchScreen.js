import React, {Component, Fragment} from 'react';
import {
    ActivityIndicator,
    Animated,
    Dimensions,
    FlatList,
    Text,
    View
} from 'react-native';
import HTTP, {baseUrl} from '../utils/HTTP'
import JobItem from "../components/Job/JobItem";
import moment from "moment";
import {bookmarkAdd, bookmarkRemove, getCategories, getJob, getJobs, getTypes} from "../services";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {removeBookmark, setBookmark, setInitialBookmarks} from "../actions/bookmarks";
const { width, height } = Dimensions.get('window');


class HomeScreen extends Component {
    constructor(){
        super()
        this.filterAnimation = new Animated.Value(0);
        this.state = {
            data: [],
            page: 0,
            loading: true,
            loadingMore: false,
            filtering: false,
            refreshing: false,
            isFilterOpened: false,
            error: null,
        };
    }


    async componentDidMount() {
        this._fetchAll();
    }


    _fetchAll = () => {
        const { page } = this.state;

        HTTP.get( '/history',{
            params:{
                offset:page,
            }
        })
            .then(response => {
                // console.log(response.data.data.jobs);
                const jobHistory = response.data.data.map(item => {
                                return {
                                    acceptStatus: item.status,
                                    ...item.job,
                                    ...item.job.data,
                                    id:item.id
                                }

                            })
                this.setState((prevState, nextProps) => ({
                    data:
                        page === 0
                            ? jobHistory
                            : [...this.state.data, ...jobHistory],
                    loading: false,
                    loadingMore: false,
                    refreshing: false
                }));
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });
    };

    _handleRefresh = () => {
        this.setState(
            {
                page: 0,
                refreshing: true
            },
            () => {
                this._fetchAll();
            }
        );
    };

    _handleLoadMore = () => {
        this.setState(
            (prevState, nextProps) => ({
                page: prevState.page + 15,
                loadingMore: true
            }),
            () => {
                this._fetchAll();
            }
        );
    };

    _renderFooter = () => {
        if (!this.state.loadingMore) return null;

        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    height: height,
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    marginTop: 10,
                    marginBottom: 10,
                    borderColor: 'transparent'
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    handleBlockPress = (data) => {
        getJob(data.slug).then(({data: {data: {first}}}) => {
            this.props.navigation.navigate('JobSingle', {data: Object.assign(first.data, data)})
        })

    };
    addBookmark = async (item) => {
        try {
            this.props.setBookmark({[item.jobId]: item});
            await bookmarkAdd(item.jobId)

        } catch (e) {
            console.log(e);

        }
    }


    rB = async (item) => {
        try {
            this.props.removeBookmark(item.jobId);
            await bookmarkRemove(item.jobId)
        } catch (e) {
            console.log(e);
        }

    }

    render() {
        return(<Fragment>
            {!this.state.loading ? (
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <JobItem
                            image={item.logo && item.logo.length > 0 ? {uri: `${baseUrl}storage/logos/${item.logo}`} : null}
                            key={item.jobId}
                            title={item.title}
                            subTitle={item.organization}
                            date={moment(item.updated_at).format('DD MMM YYYY')}
                            onBlockPress={this.handleBlockPress.bind(null, item)}
                            setOrRemoveBookmark={() => {
                                !Object.keys(this.props.bookmarks).includes(`${item.jobId}`) ?
                                    this.addBookmark(item) :
                                    this.rB(item)
                            }}
                            bookMark={Object.keys(this.props.bookmarks).includes(`${item.jobId}`)}
                            acceptStatus={item.acceptStatus || false}
                        />
                    )}
                    keyExtractor={item => item.id.toString()}
                    extraData={this.props.bookmarks}
                    // ListHeaderComponent={this._renderHeader}
                    ListFooterComponent={this._renderFooter}
                    onRefresh={this._handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReached={this._handleLoadMore}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={15}
                />
            ) : (
                <View>
                    <Text style={{ alignSelf: 'center' }}>Loading</Text>
                    <ActivityIndicator />
                </View>
            )}
        </Fragment>)
    }
}
export default connect(state => state, d => bindActionCreators({
    setBookmark,
    removeBookmark,
    getJobs,
    setInitialBookmarks
}, d))(HomeScreen)
