import React, {Fragment, useEffect, useState} from "react";
import {
    BackHandler,
    Dimensions,
    Image,
    ImageBackground,
    Modal,
    ScrollView,
    Share,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import Container from "../components/Container";
import Ionicons from "react-native-vector-icons/Ionicons";
import {isTablet, ratio, screenWidth} from "../utils/Styles";

import ButtonGradient from "../components/ButtonGradient";
import CustomIcon from "../utils/CustomIcon";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {removeBookmark, setBookmark} from "../actions/bookmarks";
import HTTP, {baseUrl} from "../utils/HTTP";
import LoadingActivityView from "../LoadingActivityView";
import {applyJob, bookmarkAdd, bookmarkRemove} from "../services";
import {translate} from "../utils/translations";
import ButtonGradientBlue from "../components/ButtonGradient/Blue";
import RNPickerSelect from 'react-native-picker-select';
import DocumentPicker from "react-native-document-picker";
import * as RNFS from "react-native-fs";
import Select from "../components/Input/Select";
import {Popup} from "popup-ui";
const SCREEN_WIDTH = Dimensions.get('window').width
const MARGIN_SMALL = 8
const MARGIN_LARGE = 16

const styles = StyleSheet.create({
    image: {
        width: '100%',
        padding: 0,
    },
    container: {
        flex: 1,
        width: '100%',
        padding: 10 * ratio,
        marginLeft: 5 * ratio
    },
    text: {
        fontSize: 16 * ratio,
        color: '#188ca7',
        textAlign: 'center',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    line: {
        backgroundColor: '#fff',
        height: 3 * ratio,

    },
    imageProfileContainer: {
        width: 101 * ratio,
        height: 81 * ratio,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#DDDDDD',
        backgroundColor: '#fff',
        marginTop: 20 * ratio,
        // position: 'absolute',
        // bottom: -30,
        zIndex: 100
    },
    subtitle: {
        fontWeight: 'bold'
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        minHeight: 30
    },
    subTitleMessage: {
        textAlign: 'left',
        width: screenWidth / 3,
        maxWidth: screenWidth / 3,
    },
    scrollViewContentContainer: {
        flex: 1,
        width: SCREEN_WIDTH,
        padding: MARGIN_LARGE,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    label: {
        fontSize: 13,
        marginTop: MARGIN_LARGE
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    smallInputWrapper: {
        flexDirection: 'column'
    },
    selectInput: {
        flexDirection: 'row',
        height: 36,
        borderWidth: 1,
        borderRadius: 4,
        padding: MARGIN_SMALL,
        marginTop: MARGIN_LARGE,
        backgroundColor: '#FFFFFF'
    },
    selectInputSmall: {
        width: SCREEN_WIDTH * 0.5 - MARGIN_LARGE * 2
    },
    selectInputLarge: {
        width:'100%'
    },
    bananawrapper: {
        margin: MARGIN_LARGE * 2,
        marginBottom: 0,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    selectInputInner: {
        height: 36,
        borderRadius: 4
    },

})
const JobSingle = (props) => {
    const [modalVisible,setModalVisible] = useState(false);
    const [resumes, setResumes] = useState([])
    const [selectedResume, setSelectedResume] = useState(null);
    const [uploadedResume, setUploadedResume] = useState({});
    const data = props.navigation.getParam('data') || {}
    const [isLoading, setIsLoading] = useState(false)
    let h = isTablet ? screenWidth / 4.2 * ratio : 118 * ratio;
    useEffect(() => {
        HTTP.get('/jobs').then(({data: {data: {resumes}}}) => {
            const r = resumes.map((item,index) => ({value:item.id,label:translate('resume') + ' ' + (index + 1),...item }))
            setResumes(r)
        })
    },[])
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    const handleSubmit = async () => {
        setIsLoading(true)

            let dataClone;
            try {
                if(selectedResume){
                    dataClone = {
                        job_id:data.job_id,
                        choose_resume:selectedResume
                    }
                }else{
                    dataClone = {
                        job_id:data.job_id,
                        ...uploadedResume
                    }
                }
                setModalVisible(false)
                await applyJob(dataClone)
                Popup.show({
                    type: 'Success',
                    title:translate('applyed_sucess'),
                    button: false,
                    textBody: translate('applyed_sucess_job'),
                    buttontext: translate('ok'),
                    callback: () => Popup.hide(),
                })
                setIsLoading(false)
                setModalVisible(false)
            } catch (e) {
                setIsLoading(false)
                setModalVisible(false)
                console.log(e);
            }

    }
    const addBookmark = async (item) => {
        try {
            props.setBookmark(item);
            await bookmarkAdd(item[Object.keys(item)[0]].jobId)

        } catch (e) {
            console.log(e);
        }

    }

    const br = async (id) => {
        try {
            props.removeBookmark(id);
            await bookmarkRemove(id)
        } catch (e) {
            console.log(e);
        }

    }

    return (
        <Container style={{justifyContent: 'flex-start'}}>
            <View style={[styles.image,]}>
                <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                                 source={require('../components/Navigation/img/home_gradient.png')}
                                 resizeMode={"cover"}>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <Ionicons name="ios-arrow-back" size={27 * ratio} color='#fff'/>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: 20}}>
                        <View style={styles.imageProfileContainer}>
                            <Image
                                source={data.logo ? {uri: `${baseUrl}/storage/${data.logo}`} : require('../components/Job/img/company.png')}
                                style={{width: '100%', height: "100%"}} resizeMode="contain"/>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 'auto', paddingBottom: 8 * ratio}}>
                            <TouchableOpacity onPress={async () => {
                                try {
                                    const result = await Share.share({
                                        title:'barev',
                                        message:data.description,
                                        url:'https://asdasd.asdas'
                                    });

                                    if (result.action === Share.sharedAction) {
                                        if (result.activityType) {
                                            // shared with activity type of result.activityType
                                        } else {
                                            // shared
                                        }
                                    } else if (result.action === Share.dismissedAction) {
                                        // dismissed
                                    }
                                } catch (error) {
                                    alert(error.message);
                                }
                            }} style={{paddingHorizontal: 10}}>
                                <CustomIcon name="share-button" size={25} color='#fff'/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                !Object.keys(props.bookmarks).includes(`${data.job_id}`) ?
                                    addBookmark({
                                        [data.job_id]: {
                                            jobId: data.job_id,
                                            slug: data.id,
                                            updated_at: data.updated_at,
                                            title: data.title,
                                            organization: data.organization,
                                            logo: data.logo
                                        }
                                    }) :
                                    br(data.job_id)
                            }}>
                                {Object.keys(props.bookmarks).includes(`${data.job_id}`) ?
                                    (<CustomIcon name="like-heart" size={25} color='#fff'/>) :
                                    (<CustomIcon name="heart" size={25} color='#fff'/>)
                                }

                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
                <View style={{paddingLeft: 120 * ratio, paddingTop: 10}}>
                    <Text style={[styles.text, {marginBottom: 5}]}>{data.organization}</Text>
                    <Text style={[styles.text, {marginBottom: 5}]}>{data.deadline}</Text>
                </View>
            </View>
            <ScrollView style={{flex: 1}} contentContainerStyle={{paddingVertical: 40}}>
                <View style={{flex: 1, paddingHorizontal: 20}}>
                    <Text style={{fontSize: 16 * ratio, fontWeight: 'bold', marginBottom: 20}}>{data.menu}</Text>
                    <View style={{flex: 1}}>
                        <View style={styles.listItem}>
                            <Text style={styles.subtitle}>Company:</Text>
                            <Text style={styles.subTitleMessage}>{data.organization}</Text>
                        </View>
                        <View style={styles.listItem}>
                            <Text style={styles.subtitle}>Location:</Text>
                            <Text style={styles.subTitleMessage}>{data.location_city}</Text>
                        </View>
                        {data.start_date ? (<View style={styles.listItem}>
                            <Text style={styles.subtitle}>Start Date/time:</Text>
                            <Text style={styles.subTitleMessage}>{data.start_date}</Text>
                        </View>) : null}
                        {data.duration ? (<View style={styles.listItem}>
                            <Text style={styles.subtitle}>Duration:</Text>
                            <Text style={styles.subTitleMessage}>{data.duration}</Text>
                        </View>) : null}
                        {data.description && (<Fragment>
                        <Text style={[styles.subtitle, {marginTop: 20 * ratio}]}>Job Description:</Text>
                        <Text style={{marginTop: 10 * ratio}}>{data.description}</Text>
                        </Fragment>)}
                        {data.responsibilities && (<Fragment>
                        <Text style={[styles.subtitle, {marginTop: 20 * ratio}]}>Job Responsibilities:</Text>
                        <Text style={{marginTop: 10 * ratio}}>{data.responsibilities}</Text>
                        </Fragment>)}
                        {data.requirements && (<Fragment>
                        <Text style={[styles.subtitle, {marginTop: 20 * ratio}]}>Familiarity With:</Text>
                        <Text style={{marginTop: 10 * ratio}}>{data.requirements}</Text>
                        </Fragment>)}
                        <ButtonGradient text={translate('apply_now')} onPress={() =>setModalVisible(true)}
                                        style={{marginLeft: 'auto', marginRight: 'auto', marginTop: 25 * ratio}}/>
                    </View>
                </View>
            </ScrollView>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                }}>
                <View style={{flex:1,justifyContent:'center',backgroundColor:'rgba(0,0,0,0.49)',alignItems:'center'}}>
                    <View style={{width:"90%",borderRadius:4,backgroundColor:'#fff',padding:10}}>
                        <Text style={{fontWeight:'bold',textAlign:'center',marginBottom:10,fontSize:20}}>{translate('select_or_upload')}</Text>
                        <Select
                            objectRecompose={{key:'value',label:'label'}}
                            onValueChange={(value) =>  setSelectedResume(value)}
                            value={selectedResume}
                            items={resumes}
                            placeholder={translate('select_resume')}
                        />
                        <View style={{marginVertical:20,flexDirection:'row',justifyContent:'space-between'}}>

                            <TouchableOpacity  onPress={async () => {
                                if (selectedResume) {
                                    return;
                                }
                                try {
                                    const res = await DocumentPicker.pick({
                                        type: [DocumentPicker.types.allFiles],
                                    });
                                    const r = await RNFS.readFile(res.uri,'base64')
                                    const file = `data:${res.type};base64,${r}`;

                                    setUploadedResume({file,name:res.name})
                                } catch (err) {
                                    if (DocumentPicker.isCancel(err)) {
                                        // User cancelled the picker, exit any dialogs or menus and move on
                                    } else {
                                        console.log(err)
                                        throw err;
                                    }
                                }
                            }} style={{position:'absolute',left:0,top:-5,maxHeight:30}}>
                                <Text style={{color:selectedResume ? 'grey' :'#1fa091',textDecorationLine:'underline'}}>
                                    {translate('upload_resume')}
                                </Text>
                            </TouchableOpacity>
                            <Text style={{marginTop:20}}>{uploadedResume.name}</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <ButtonGradientBlue text={translate('cancel')} onPress={() =>setModalVisible(false)} />
                            <ButtonGradient text={translate('apply')} onPress={handleSubmit} />

                        </View>
                    </View>
                </View>
            </Modal>
            <LoadingActivityView visible={isLoading}/>
        </Container>
    )
}

export default connect(state => state, d => bindActionCreators({
    setBookmark,
    removeBookmark
}, d))(React.memo(JobSingle))
