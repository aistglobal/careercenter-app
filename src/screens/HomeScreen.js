import React, {Component, Fragment} from 'react';
import {
    ActivityIndicator,
    Animated,
    Dimensions,
    Easing,
    FlatList,
    ImageBackground, Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import HTTP, {baseUrl} from '../utils/HTTP'
import JobItem from "../components/Job/JobItem";
import moment from "moment";
import {bookmarkAdd, bookmarkRemove, getCategories, getJob, getJobs, getTypes} from "../services";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {removeBookmark, setBookmark, setInitialBookmarks} from "../actions/bookmarks";
import {isTablet, ratio, screenHeight, screenWidth} from "../utils/Styles";
import CustomIcon from "../utils/CustomIcon";
import {translate} from "../utils/translations";
import {Searchbar} from "react-native-paper";
import Select from "../components/Select";
// screen height and width
const { width, height } = Dimensions.get('window');
let navImage = require('../components/Navigation/img/home_gradient.png');
let h = isTablet  ? screenWidth / 4.2 * ratio : 118 * ratio;
const menuWidth = screenWidth - 50 * ratio;

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: '100%',
        padding: 0,
        height: 200
    }, container: {
        flex: 1,
        width: '100%',
        padding: 15 * ratio,
    },
    text: {
        fontSize: 28 * ratio,
        color: '#fff',
        textAlign: 'center',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    line: {
        backgroundColor: '#fff',
        height: 3 * ratio,

    },
    profileImage: {
        width: 73 * ratio,
        height: 73 * ratio,
        borderRadius: 100,
        borderWidth: 0.5,
        padding: 0.5,
        borderColor: '#DDDDDD',
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: -31 * ratio,
        left: 20 * ratio,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    profileText: {
        marginLeft: 100 * ratio,
        marginRight: 'auto',
        top: 25 * ratio
    }
});

class HomeScreen extends Component {
    constructor(){
        super()
        this.filterAnimation = new Animated.Value(0);
        this.state = {
            data: [],
            page: 0,
            loading: true,
            loadingMore: false,
            filtering: false,
            refreshing: false,
            isFilterOpened: false,
            error: null,
            searchQuery:{},
            firstOpen:true,
            categories:[]
        };
    }


    async componentDidMount() {
        this._fetchAll();
        const {data:{data:types}} =  await getTypes();
        const typesObject= {};
        types.map(item => Object.assign(typesObject,item));
        const t = Object.keys(typesObject).map(item => ({label:typesObject[item],id:item}));
        const {data:{data:categories}} =  await getCategories();
        this.setState({types:t,categories})
    }

    search = async (clear = false) => {
        if (clear) {
            this.setState({searchQuery:{}},() => this._fetchAll())
        }
        try {
            this.setState({page:0},() => this._fetchAll())
            this.animate();
        } catch (e) {
            console.log(e);
        }
    }
    _fetchAll = () => {
        const { page,searchQuery } = this.state;
            console.log(searchQuery)
        HTTP.get( '/jobs',{
            params:{
                    offset:page,
                    ...searchQuery
            }
        })
            .then(response => {
                this.setState((prevState, nextProps) => ({
                    data:
                        page === 0
                            ? response.data.data.jobs
                            : [...this.state.data, ...response.data.data.jobs],
                    loading: false,
                    loadingMore: false,
                    refreshing: false
                }));
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });
    };
     animate = () => {

        this.filterAnimation.setValue(0);
        this.setState({isFilterOpened:!this.state.isFilterOpened});
        Animated.timing(
            this.filterAnimation,
            {
                toValue: 1,
                duration: 200,
                easing: Easing.linear
            }
        ).start(() => this.setState({firstOpen:false}))
    }
    _handleRefresh = () => {
        this.setState(
            {
                page: 0,
                refreshing: true
            },
            () => {
                this._fetchAll();
            }
        );
    };

    _handleLoadMore = () => {
        this.setState(
            (prevState, nextProps) => ({
                page: prevState.page + 15,
                loadingMore: true
            }),
            () => {
                this._fetchAll();
            }
        );
    };

    _renderFooter = () => {
        if (!this.state.loadingMore) return null;

        return (
            <View
                style={{
                    position: 'relative',
                    width: width,
                    height: height,
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    marginTop: 10,
                    marginBottom: 10,
                    borderColor: 'transparent'
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    handleBlockPress = (data) => {
        getJob(data.slug).then(({data: {data: {first}}}) => {
            this.props.navigation.navigate('JobSingle', {data: Object.assign(first.data, data)})
        })

    };
    addBookmark = async (item) => {
        try {
            this.props.setBookmark({[item.jobId]: item});
            await bookmarkAdd(item.jobId)

        } catch (e) {
            console.log(e);
        }

    }



    removeBookmark = async (item) => {
        try {
            this.props.removeBookmark(item.jobId);
            await bookmarkRemove(item.jobId)
        } catch (e) {
            console.log(e);
        }

    }
    render() {
        return(<Fragment>
            <View style={[{height:h},Platform.OS === 'ios' &&{zIndex:100}]}>
                <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                                 source={navImage}
                                 resizeMode={"cover"}>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <CustomIcon name="line-menu" size={20 * ratio} color='#fff'/>
                        </TouchableOpacity>
                        <Text style={styles.text}>{translate('job_for_everyone')}</Text>
                    </View>
                    <TouchableOpacity style={[{
                        backgroundColor: '#00c4ac',
                        width: 50 * ratio,
                        height: 50 * ratio,
                        borderRadius: 100,
                        justifyContent: 'center',
                        alignItems: 'center',
                        zIndex: 9999,
                        position: 'absolute',
                        bottom: -20,
                        right: 20
                    }]} onPress={this.animate}>
                        <CustomIcon name="filter-results-button" color="#fff" size={20 * ratio}/>
                    </TouchableOpacity>
                    <Animated.View style={{
                        position: 'absolute',
                        width: menuWidth,
                        height: screenHeight - h,
                        bottom: -screenHeight + h,
                        // right:0
                        right: this.filterAnimation.interpolate({
                            inputRange: [0, 1],
                            outputRange: this.state.isFilterOpened ? [-menuWidth, 0] : this.state.firstOpen ? [-menuWidth, -menuWidth] : [-50 * ratio, -menuWidth]
                        })
                    }}><ImageBackground source={require('../components/Navigation/img/menuGradient.png')}
                                        style={{width: '100%', height: '100%', paddingTop: 20 * ratio,zIndex:100}}>
                        <View style={{
                            width: "100%",
                            height: '100%',
                            flex: 1,
                            paddingBottom: 20 * ratio,
                            justifyContent: 'space-between',
                            zIndex:2,
                            marginBottom:50
                        }}>
                            <ScrollView>
                                <Searchbar onIconPress={() => this.search(false)}
                                           onChangeText={search => this.setState({searchQuery:{...this.state.searchQuery, search}})}
                                           placeholder={translate("search")}
                                           style={{
                                               width: '90%',
                                               marginLeft: 'auto',
                                               marginRight: 'auto',
                                           }}
                                           value={this.state.searchQuery.search}

                                />
                                <Select title={translate("sort_by")}
                                        items={this.state.types}
                                        value="id"
                                        selected={this.state.searchQuery.type || []}
                                        onChange={type => this.setState({searchQuery:{...this.state.searchQuery, type}})}
                                />
                                <Select title={translate("categories")}
                                        value="value"
                                        returnOne
                                        selected={this.state.searchQuery.category || []}
                                        items={Object.keys(this.state.categories).map(item => ({label: item, value: item}))}
                                        onChange={category => {
                                            this.setState({searchQuery:{...this.state.searchQuery, category:category.length > 0 ? category : null}})
                                        }}
                                />
                                {this.state.searchQuery.category && (<Select title={translate("sub_categories")}
                                                                  value="value"
                                                                  returnOne
                                                                  selected={this.state.searchQuery.sub_category || []}
                                                                  items={this.state.categories[this.state.searchQuery.category[0]].map(item => ({
                                                                      label: item,
                                                                      value: item
                                                                  }))}
                                                                  onChange={sub_category => this.setState({searchQuery:{
                                                                      ...this.state.searchQuery,
                                                                      sub_category
                                                                  }})}
                                />)}

                            </ScrollView>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <TouchableOpacity style={{padding: 20 * ratio}} onPress={() => this.search(true)}>
                                    <Text style={{color: '#fff'}}>{translate('clear_filter')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{padding: 20 * ratio}}
                                                  onPress={() => this.search(false)}>
                                    <Text style={{color: '#fff'}}>{translate('apply_filter')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </ImageBackground>
                    </Animated.View>
                </ImageBackground>
            </View>
         {!this.state.loading ? (
            <FlatList
                data={this.state.data}
                renderItem={({ item }) => (
                    <JobItem
                        image={item.logo && item.logo.length > 0 ? {uri: `${baseUrl}/storage/${item.logo}`} : null}
                        key={item.jobId}
                        title={item.title}
                        subTitle={item.organization}
                        date={moment(item.updated_at).format('DD MMM YYYY')}
                        onBlockPress={this.handleBlockPress.bind(null, item)}
                        setOrRemoveBookmark={() => {
                            !Object.keys(this.props.bookmarks).includes(`${item.jobId}`) ?
                                this.addBookmark(item) :
                                this.removeBookmark(item)
                        }}
                        bookMark={Object.keys(this.props.bookmarks).includes(`${item.jobId}`)}
                        acceptStatus={item.acceptStatus || false}
                    />


                )}
                keyExtractor={item => item.jobId.toString()}
                extraData={this.props.bookmarks}
                // ListHeaderComponent={this._renderHeader}
                ListFooterComponent={this._renderFooter}
                onRefresh={this._handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={15}
            />
        ) : (
            <View>
                <Text style={{ alignSelf: 'center' }}>Loading</Text>
                <ActivityIndicator />
            </View>
        )}
        </Fragment>)
    }
}
export default connect(state => state, d => bindActionCreators({
    setBookmark,
    removeBookmark,
    getJobs,
    setInitialBookmarks
}, d))(HomeScreen)
