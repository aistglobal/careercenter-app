import React, {useEffect} from "react";
import {BackHandler, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity} from "react-native";
import store from 'react-native-simple-store'
import Container from "../components/Container";
import {ratio} from "../utils/Styles";
import CustomIcon from "../utils/CustomIcon";
import * as auth from '../services/auth';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {logout} from "../actions/user";
import {translate} from "../utils/translations";


const styles = StyleSheet.create({
    list: {
        flex: 1,
        width: '100%',
        padding: 20 * ratio
    },
    listItem: {
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        // justifyContent:'center',
        height: 50 * ratio,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(112,112,112,0.27)'

    },
    listItemText: {
        marginLeft: 20 * ratio,
        fontSize: 14 * ratio,
    }
})
const ProfileScreen = (props) => {
    useEffect(() => {
      props.getUser()
    }, []);
    const backHandler = () =>{
        props.navigation.goBack()
        return true
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress',backHandler)
        return () => {
            BackHandler.removeEventListener('hardwareBackPress',backHandler)
        }
    },[])
    return (
        <Container>
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff', width: '100%', justifyContent: 'center'}}>
                <ScrollView style={styles.list} contentContainerStyle={{paddingVertical: 20 * ratio}}>
                    <TouchableOpacity style={styles.listItem} onPress={() =>  props.navigation.navigate('CvScreen')}>
                        <CustomIcon name="cv" size={20 * ratio} color="#005E9D"/>
                        <Text style={styles.listItemText}>{translate('My_CV')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listItem}
                                      onPress={() => props.navigation.navigate('JobSearchScreen')}
                    >
                        <CustomIcon name="passage-of-time" size={20 * ratio} color="#005E9D"/>
                        <Text style={styles.listItemText}>{translate('job_history')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listItem}
                                      onPress={() => props.navigation.navigate('BookmarksScreen')}
                    >
                        <CustomIcon name="like-heart" size={20 * ratio} color="#005E9D"/>
                        <Text style={styles.listItemText}>{translate('favourites')}</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity style={styles.listItem}>*/}
                    {/*    <CustomIcon name="settings" size={20 * ratio} color="#005E9D"/>*/}
                    {/*    <Text style={styles.listItemText}>Profile settings</Text>*/}
                    {/*</TouchableOpacity>*/}
                    {/*<TouchableOpacity style={styles.listItem}>*/}
                    {/*    <CustomIcon name="download" size={20 * ratio} color="#005E9D" bold/>*/}
                    {/*    <Text style={styles.listItemText}>Email CV</Text>*/}
                    {/*</TouchableOpacity>*/}
                    <TouchableOpacity style={[styles.listItem, {borderBottomColor: 'transparent'}]}
                                      onPress={props.logout}>
                        <CustomIcon name="sign-out" size={20 * ratio} color="#005E9D"/>
                        <Text style={styles.listItemText}>{translate('sign_out')}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        </Container>
    )
}

export default connect(state => state,dispatch =>
    bindActionCreators({
        getUser:auth.getUser,
        logout:logout
},dispatch))(React.memo(ProfileScreen))
