import React, {Component} from "react";
import {Animated, Easing, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import CustomIcon from "../../utils/CustomIcon";
import {ratio} from "../../utils/Styles";

const style = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width:"90%",
        marginLeft:'auto',
        marginRight:'auto',
        paddingHorizontal: 10 * ratio,
        height: 50 * ratio,
        borderBottomColor: 'rgba(255,255,255,0.44)',
        borderBottomWidth: 0.5
    }
})

class Select extends Component {
    static defaultProps = {
        selected:[]
    }
    constructor(props) {
        super(props);
        this.filterAnimation = new Animated.Value(0)
        this.state = {
            isFirstOpen: true,
        }
    }


    animate = () => {
        this.filterAnimation.setValue(0)
        this.setState({isOpen: !this.state.isOpen})
        Animated.timing(
            this.filterAnimation,
            {
                toValue: 1,
                duration: 200,
                easing: Easing.linear
            }
        ).start(() => this.setState({isFirstOpen: false}))
    }

    render() {
        const {title,value,selected ,returnOne,items = []} = this.props
        const height = this.filterAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.isOpen ? [0, items.length * 50 * ratio] : this.state.isFirstOpen ? [0, 0] : [items.length * 50 * ratio, 0]
        })
        const opacity = this.filterAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: this.state.isOpen ? [0,  1] : this.state.isFirstOpen ? [0, 0] : [1, 0]
        })
        return (
            <View>
                <TouchableOpacity onPress={this.animate}>
                    <View style={style.row}>
                        <Text style={{color: '#fff'}}>{title}</Text>
                        <CustomIcon name={this.state.isOpen ? "arrow-up" :"arrow-down" } color="#fff" size={7}/>
                    </View>
                </TouchableOpacity>
                <Animated.View style={{height}}>
                    {items.map(item => {
                        return (
                            <TouchableOpacity onPress={() => {
                                    this.props.onChange(selected.includes(item[value]) ?
                                        selected.filter(key => key != item[value]):
                                        returnOne ? [item[value]] : selected.concat(item[value]));
                            }}
                            key={item[value]}
                            >
                            <Animated.View style={[style.row, {height: 50 * ratio,
                                opacity,
                                backgroundColor:'#fff',
                                borderBottomColor: 'rgba(112,112,112,0.36)',
                                borderBottomWidth: 1,
                                width:'100%',
                                paddingRight:23 * ratio
                            }]}>
                                <Text>{item[this.props.label] || item.label}</Text>
                                <IonIcon
                                    name={selected.includes(item[this.props.value]) ?
                                        "ios-radio-button-on" :"ios-radio-button-off"}
                                    color="#707070" size={25}/>

                            </Animated.View></TouchableOpacity>)
                    })}
                </Animated.View>
            </View>
        )
    }
}

export default Select
