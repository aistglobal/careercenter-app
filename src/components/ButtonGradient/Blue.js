/* eslint-disable import/named */
import React from 'react';
import {StyleSheet, Text, TouchableHighlight, Dimensions, View, TouchableOpacity} from 'react-native';
// import { LinearGradient } from 'expo';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import {ratio} from "../../utils/Styles";
const BORDER_RADIUS = 50
const HEIGHT = 50 * ratio
const styles = StyleSheet.create({
    button: {
        width: 116 * ratio,
        height: HEIGHT,
        borderRadius: BORDER_RADIUS,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const ButtonGradientBlue = ({ style, onPress, text }) => (
    <TouchableOpacity style={[styles.button, style]} onPress={onPress} >

        <LinearGradient
            colors={[ '#0071B4','#0CAE9A',]}
            style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                height: HEIGHT,
                borderRadius: BORDER_RADIUS,
                justifyContent: 'center',
                alignItems: 'center',
            }}
            start={{ x: 1, y: 0.5 }}
            end={{ x: 0.1, y: 1 }}
            locations={[0, 1]}
        >
            <Text style={{ color: '#fff' }}>{text}</Text>
        </LinearGradient>

    </TouchableOpacity>
);
ButtonGradientBlue.propTypes = {
    styles: PropTypes.object,
    onPress: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
};
export default ButtonGradientBlue;
