import React, {useState} from "react";
import {ActivityIndicator, Image, Share, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {ratio} from '../../utils/Styles'
import CustomIcon from "../../utils/CustomIcon";
import {translate} from "../../utils/translations";

const style = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#D5D3D3',
        flexDirection: 'row',
        // maxHeight: 100,
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    },
    title: {
        fontWeight: '600',
        fontSize: 15 * ratio,
        padding: 3 * ratio,
    },
    subTitle: {
        color: 'rgba(0,0,0,0.33)',
        fontSize: 14 * ratio,
        padding: 3 * ratio,
    },
    date: {
        color: "#35BBAB",
        padding: 3 * ratio,
    },
    shareActions: {
        // flex:2,
        // marginTop: 'auto',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    icon: {
        padding: 5
    }
})
const JobItem = (props) => {
    const [isImageLoading, setImageLoading] = useState(false)
    return (<TouchableOpacity style={[style.container]} onPress={props.onBlockPress}>

        <View style={{flex: 1, paddingRight: 10 * ratio, height: '100%'}}>
            <Image source={props.image || require('./img/company.png')} style={{width: '100%', marginBottom: 'auto',height:'100%'}}
                   resizeMode={"contain"} onLoadStart={() => setImageLoading(true)}
                   onLoadEnd={() => setImageLoading(false)}/>
            {isImageLoading && <ActivityIndicator style={{position: 'absolute', left: '40%', top: '40%'}}/>}
        </View>
        <View style={{flex: 3}}>
            <Text style={style.title}>{props.title}</Text>
            <View style={{flex: 1, justifyContent: 'space-between'}}>
                    <Text style={style.subTitle}>{props.subTitle}</Text>
                <View style={style.shareActions}>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Text style={style.date}>{props.date}</Text>
                    </View>

                    {!props.hideBookmark &&<View style={{flexDirection:'row'}}>
                        <TouchableOpacity onPress={async () => {
                            try {
                                const result = await Share.share({
                                    message:
                                        'React Native | A framework for building native apps using React',
                                });

                                if (result.action === Share.sharedAction) {
                                    if (result.activityType) {
                                        // shared with activity type of result.activityType
                                    } else {
                                        // shared
                                    }
                                } else if (result.action === Share.dismissedAction) {
                                    // dismissed
                                }
                            } catch (error) {
                                alert(error.message);
                            }
                        }}>
                            <CustomIcon name="share-button" size={25} color='#005E9D'
                                        style={[style.icon]}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={props.setOrRemoveBookmark}>
                            {props.bookMark ?
                                (<CustomIcon name="like-heart" size={25} color='#005E9D' style={style.icon}/>) :
                                (<CustomIcon name="heart" size={25} color='#005E9D' style={style.icon}/>)
                            }

                        </TouchableOpacity>

                    </View>}

                </View>
            </View>
            {props.acceptStatus && makeJobStatus(props.acceptStatus)}
        </View>
    </TouchableOpacity>)
};
function makeJobStatus(status){
    let color = '';
    let statusText = translate(status);
    switch (status) {
        case'pending' :
             color = 'green';
            break;
        case'accomplished' :
             color = '#0b5496';
            break;
        case'rejected' :
             color = 'red';
            break;
        case'refused' :
             color = 'red';
            break;
    }
    return <Text style={{width:'100%',color}}>{statusText}</Text>
}
export default JobItem
