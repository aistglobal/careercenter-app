import React, {Fragment, useEffect, useState} from "react";
import {Dimensions, Text, View} from "react-native";
import JobItem from "./JobItem";
import {bookmarkAdd, bookmarkRemove, getBookmarks, getJob, getJobs} from "../../services";
import {baseUrl} from "../../utils/HTTP";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {removeBookmark, setBookmark, setInitialBookmarks} from "../../actions/bookmarks";
import {ActivityIndicator} from "react-native-paper";
import {ratio} from "../../utils/Styles";
import {translate} from "../../utils/translations";
import moment from "moment";

const JobItems = (props) => {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        (async () => {
            try {
                const j =  await props.getJobs({offset:15});
                const {data: {data: bookmarks}} = await getBookmarks();
                const bMarks = {}
                const b = bookmarks.map(item => item.job_id)
                const jobs = j.filter(item => b.includes(item.jobId))
                await jobs.map(item => {
                    Object.assign(bMarks, {[item.jobId]: item})
                })
                console.log('bMarks',bMarks)
                props.setInitialBookmarks(bMarks)
                setIsLoading(false)
            } catch (e) {
                setIsLoading(false)
            }

        })()

    }, []);



    const jobs = props.isBookmarks ? props.bookmarkedJobs : props.jobs

    const handleBlockPress = (data) => {
        getJob(data.slug).then(({data: {data: {first}}}) => {
            props.navigation.navigate('JobSingle', {data: Object.assign(first.data, data)})
        })

    };
    const addBookmark = async (item) => {
        try {
            props.setBookmark({[item.jobId]: item});
            await bookmarkAdd(item.jobId)

        } catch (e) {
            console.log(e);
        }

    }
    const _renderFooter = () => {
        if (!pagination.loadingMore) return null;

        return (
            <View
                style={{
                    position: 'relative',
                    width: '100%',
                    height: '100%',
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    marginTop: 10,
                    marginBottom: 10,
                    borderColor: 'red'
                }}
            >
                <ActivityIndicator animating size="large"/>
            </View>
        );
    };

    const removeBookmark = async (item) => {
        try {
            props.removeBookmark(item.jobId);
            await bookmarkRemove(item.jobId)
        } catch (e) {
            console.log(e);
        }

    }

    if (isLoading) {
        return (<View
            style={{height: Dimensions.get("window").height - 200, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size={"large"} color='#0AA39F'/>
        </View>)
    }

    if (jobs.length === 0) {
        return (<View style={{height: Dimensions.get("window").height - 200, justifyContent: 'center'}}>
            <Text style={{color: '#0AA39F', fontWeight: 'bold', fontSize: 17 * ratio, textAlign: 'center'}}>
                {props.noResultMessage || translate('results_not_found')}
            </Text>
        </View>)
    }

    return (
        <Fragment>
            {jobs.map(item => (<JobItem
                image={item.logo && item.logo.length > 0 ? {uri: `${baseUrl}/storage/${item.logo}`} : null}
                key={item.jobId}
                title={item.title}
                subTitle={item.organization}
                date={moment(item.updated_at).format('DD MMM YYYY')}
                onBlockPress={handleBlockPress.bind(null, item)}
                setOrRemoveBookmark={() => {
                    !Object.keys(props.bookmarks).includes(`${item.jobId}`) ?
                        addBookmark(item) :
                        removeBookmark(item)
                }}
                bookMark={Object.keys(props.bookmarks).includes(`${item.jobId}`)}
                acceptStatus={item.acceptStatus || false}
            />))
            }
        </Fragment>
    )
}

export default connect(state => state, d => bindActionCreators({
    setBookmark,
    removeBookmark,
    getJobs,
    setInitialBookmarks
}, d))(React.memo(JobItems))
