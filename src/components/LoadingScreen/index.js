import React from "react";
import {ActivityIndicator, ImageBackground} from "react-native";




export default () => {
    return (
        <ImageBackground source={require('./img/loading_screen.png')} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <ActivityIndicator style={{marginTop:'60%'}} size="large"  color={'#fff'}/>
        </ImageBackground>
    )
}

