import React, {Fragment, useEffect, useState} from 'react'
import {
    Animated,
    Easing,
    Image,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    NativeModules, Platform
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {b64toBlob, isTablet, ratio, screenHeight, screenWidth} from '../../utils/Styles'
import Ionicons from "react-native-vector-icons/Ionicons";
import CustomIcon from "../../utils/CustomIcon";
import CustomStatusBar from "../StatusBar";
import Select from "../Select";
import {connect} from "react-redux";
import {Searchbar} from "react-native-paper";
import {addImage, getCategories, getJobs, getTypes} from "../../services";
import {bindActionCreators} from "redux";
import {translate} from "../../utils/translations";
import {baseUrl} from "../../utils/HTTP";
const ImagePicker = NativeModules.ImageCropPicker;

const options = {
    title: 'Select Avatar',
    tintColor:'#127d9f',
    cropping:true,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const styles = StyleSheet.create({
    image: {
        flex: 1,
        width: '100%',
        padding: 0,
    }, container: {
        flex: 1,
        width: '100%',
        padding: 15 * ratio,
    },
    text: {
        fontSize: 28 * ratio,
        color: '#fff',
        textAlign: 'center',
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    line: {
        backgroundColor: '#fff',
        height: 3 * ratio,

    },
    profileImage: {
        width: 73 * ratio,
        height: 73 * ratio,
        borderRadius: 100,
        borderWidth: 0.5,
        padding: 0.5,
        borderColor: '#DDDDDD',
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: -31 * ratio,
        left: 20 * ratio,
        justifyContent: 'center',
        alignItems: 'center',
        overflow:'hidden'
    },
    profileText: {
        marginLeft: 100 * ratio,
        marginRight: 'auto',
        top:20 * ratio
    }
})
const filterAnimation = new Animated.Value(0);
const HeaderWithBurger =  (props) => {
    const [isFilterOpened, setFilterOpened] = useState(false);
    const [firstOpen, setFirstOpen] = useState(true);
    const [searchQuery, setSearchQuery] = useState({});
    const [types, setTypes] = useState([]);
    const [categories, setCategories] = useState({});
    const [avatarImage, setAvatarImage] = useState(null);

    const {first_name,last_name} = props.user;

    useEffect(() => {
        (async () => {
            try {
               const {data:{data:types}} =  await getTypes();
                const typesObject= {};
                types.map(item => Object.assign(typesObject,item));
                const t = Object.keys(typesObject).map(item => ({label:typesObject[item],id:item}));
                setTypes(t);
                const {data:{data:categories}} =  await getCategories();
                setCategories(categories)

            }catch (e) {

            }
        })()
    },[]);
    function animate() {

        filterAnimation.setValue(0);
        setFilterOpened(!isFilterOpened);
        Animated.timing(
            filterAnimation,
            {
                toValue: 1,
                duration: 200,
                easing: Easing.linear
            }
        ).start(() => setFirstOpen(false))
    }
    const search = async (clear = false) => {
        console.log('asdasdsa',searchQuery,clear);
        if (clear) {
            setSearchQuery({})

        }
        try{
            await props.getJobs(clear ? {} : searchQuery);
            animate();
        }catch (e) {
            console.log(e);
        }
    }
   const uploadImage = () =>{
       ImagePicker.openPicker({
           width: 300,
           height: 400,
           cropping: true,
           includeBase64:true
       }).then(image => {
           addImage({image:`data:${image.mime};base64,`+ image.data})
           setAvatarImage({uri: `data:${image.mime};base64,`+ image.data});
       });
        // ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);
        //
        //     if (response.didCancel) {
        //         console.log('User cancelled image picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         const source = { uri: response.uri };
        //
        //         // You can also display the image using data:
        //         const sourceSow = { uri: 'data:image/jpeg;base64,' + response.data };
        //
        //         setAvatarImage(sourceSow);
        //     }
        // });
    }
    let showFilter = false;
    let isHome = false;
    let navImage = require('./img/home_gradient.png');
    let isModal = false;
    let isProfile = false;
    let isProfileEdit = false;
    let showBack = false;
    let h = isTablet ? 70 * ratio : 50 * ratio;
    const route = props.navigation.state.routes[0];
    switch (props.navigation.state.routes[0].index) {
        case 0: //Home
            console.info('screen:Home');

            h = 0;
            showFilter = false;
            isHome = false;
            navImage = require('./img/nav_bg.png');
            if (route.routes[0].routes.length > 1) {// Modal JobSingle
                showFilter = false;
                isHome = false;
                h = isTablet ? 70 * ratio : 50 * ratio;
                isModal = true
            }
            break;
        case 1: //Profile
            console.info('screen:Profile');
            h = isTablet ? screenWidth / 4.2 * ratio : 118 * ratio;
            navImage = require('./img/profile_nav.png');
            isProfile = true;
           const profileRoutes =  route.routes[1].routes.map(item => item.routeName)
            //ProfileEdit
            if (profileRoutes.includes('ProfileEditScreen')) {
                isProfileEdit = true
                showBack = true
            }
            if (profileRoutes.includes('CvScreen')) {
                showBack = true
            }
            break;
        case 2: //Announcement
            console.info('screen:Announcement');
            if (props.navigation.state.routes[0].routes[2].routes.length > 1) {
                isModal = true
            }
            showFilter = false;
            break;
        case 3: //Bookmarks
            console.info('screen:Bookmarks');
            if (route.routes[3].routes.length > 1) {
                isModal = true
            }
            break;
        case 4: //Notifications
            console.info('screen:Notifications');
            require('./img/home_gradient.png')
            break;
    }
    // Job search screen drawer navigator stack
    if (props.navigation.state.routes[0].routeName === "JobSearchScreen") {
        showFilter = true;
        if (props.navigation.state.routes.length > 1) {
            isModal = true
        }
    }
    if (isModal) {
        return null;
    }
    const menuWidth = screenWidth - 50 * ratio;

    return (
        <Fragment>
            {isProfile && Platform.OS === 'android' && <View style={{width: "100%", height: 30 * ratio, zIndex: -1,backgroundColor:'#fff'}} />}
            <ImageBackground style={[styles.image, {height: h, maxHeight: h}]}
                             source={navImage}
                             resizeMode={"cover"}>
                <View style={styles.container}>
                    {showBack ? (<TouchableOpacity onPress={() => props.navigation.navigate('ProfileScreen')}>
                        <Ionicons name="ios-arrow-back" size={27 * ratio} color='#fff'/>
                    </TouchableOpacity>) : (<TouchableOpacity onPress={() => props.navigation.openDrawer()}>
                        <CustomIcon name="line-menu" size={20 * ratio} color='#fff'/>
                    </TouchableOpacity>)}
                    {isHome && <Text style={styles.text}>{translate('job_for_everyone')}</Text>}
                </View>
                {showFilter && <TouchableOpacity style={{
                    backgroundColor: '#00c4ac',
                    width: 50 * ratio,
                    height: 50 * ratio,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 100,
                    position: 'absolute',
                    bottom: -20,
                    right: 20
                }} onPress={animate}>
                    <CustomIcon name="filter-results-button" color="#fff" size={20 * ratio} />
                </TouchableOpacity>}
                {isProfile && (<Fragment>
                    <View style={styles.profileImage}>
                        <Image source={
                            avatarImage ? avatarImage :
                            props.user.image ? {uri:baseUrl + '/storage/'+ props.user.image} :
                            require('./img/profile_default.png')}
                               style={{width: '100%',height:'100%'}} resizeMode={"cover"}/>
                    </View>
                    <View style={[styles.profileText,!isProfileEdit && {top:0}]}>
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 16 * ratio
                        }}>{isProfileEdit ? translate('personal_information') : `${first_name} ${last_name}`}</Text>
                        {isProfileEdit ? <TouchableOpacity style={{flexDirection:'row',paddingTop:10}} onPress={uploadImage}><CustomIcon name="add-image" color="#005E9D" size={25 * ratio}/><Text style={{color:'rgba(0,0,0,0.5)',marginLeft:10}}>{translate('add_photo')}</Text></TouchableOpacity> : null}
                    </View>
                    <TouchableOpacity style={{position: 'absolute', right: 10 * ratio, top: 20 * ratio}}
                                      onPress={() => props.navigation.navigate('ProfileEditScreen')}>
                        {!isProfileEdit && <Text style={{color: '#fff', fontSize: 14 * ratio}}>{translate('change_profile')}</Text>}
                    </TouchableOpacity>
                </Fragment>)}
                {showFilter && <Animated.View style={{
                    position: 'absolute',
                    width: menuWidth,
                    height:screenHeight - h,
                    bottom: -screenHeight + h,
                    // right:0
                    right:filterAnimation.interpolate({
                        inputRange: [0, 1],
                        outputRange: isFilterOpened ? [-menuWidth, 0] : firstOpen ? [-menuWidth,-menuWidth] : [-50 * ratio, -menuWidth]
                    })
                }}>

                    <ImageBackground source={require('./img/menuGradient.png')}
                                     style={{width: '100%', height: '100%', paddingTop:20 * ratio}}>
                        <View style={{
                            width: "100%",
                            height: '100%',
                            flex: 1,
                            paddingBottom: 20 * ratio,
                            justifyContent: 'space-between'
                        }}>
                            <ScrollView>
                                <Searchbar onIconPress={() => search(false)}
                                           onChangeText={search => setSearchQuery({...searchQuery,search})}
                                           placeholder={translate("search")}
                                            style={{
                                                width:'90%',
                                                marginLeft:'auto',
                                                marginRight:'auto',}}
                                           value={searchQuery.search}

                                />
                              <Select title={translate("sort_by")}
                                      items={types}
                                      value="id"
                                      selected={searchQuery.type || []}
                                      onChange={type => setSearchQuery({...searchQuery,type})}
                              />
                              <Select title={translate("categories")}
                                      value="value"
                                      returnOne
                                      selected={searchQuery.category || []}
                                      items={Object.keys(categories).map(item => ({label:item,value:item}))}
                                      onChange={category => setSearchQuery({...searchQuery,category})}
                              />
                                {searchQuery.category && (<Select title={translate("sub_categories")}
                                      value="value"
                                      returnOne
                                      selected={searchQuery.sub_category || []}
                                      items={categories[searchQuery.category[0]].map(item => ({label:item,value:item}))}
                                      onChange={sub_category => setSearchQuery({...searchQuery,sub_category})}
                              />)}

                            </ScrollView>

                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <TouchableOpacity style={{padding: 20 * ratio}} onPress={() => search(true)}>
                                    <Text style={{color: '#fff'}}>{translate('clear_filter')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{padding: 20 * ratio}}
                                                  onPress={() => search(false)}>
                                    <Text style={{color: '#fff'}}>{translate('apply_filter')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>


                    </ImageBackground>
                </Animated.View>
                }
            </ImageBackground>
            {isProfile && Platform.OS !== 'android' && <View style={{width: "100%", height: 30 * ratio, zIndex: -1,backgroundColor:'#fff'}} />}
        </Fragment>
    )


}
const mapStateToProps = state =>({
    ...state
})
export default  connect(mapStateToProps,dispatch => bindActionCreators({getJobs},dispatch))(HeaderWithBurger)
