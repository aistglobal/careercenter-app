import React, {useEffect, useState} from "react";
import {Platform, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import {ratio} from "../../utils/Styles";
import Icon from "react-native-vector-icons/Ionicons";
import DP from "react-native-datepicker";
import {translate} from "../../utils/translations";

const style = StyleSheet.create({
    container:{
        position: 'relative',
        width:'100%'
    },
    input: {
        // flex:1,
        // borderWidth: 1,
        // borderColor: '#005E9D',
        width: '100%',
        // // minHeight: 54 * ratio,
        // height: 54 * ratio,
        marginVertical: 10 * ratio,
        // borderRadius: 4,
        // padding: 10 * ratio,
        position: 'relative'
    },
    label: {
        color: '#005E9D',
        position: 'absolute',
        left: 10 * ratio,
        backgroundColor: '#fff',
        padding: 5,
        top:Platform.OS === "android" ? -4 : -10 * ratio
    }
})
const DPStyle = StyleSheet.create({
    dateTouch: {
        width: 142
    },
    // dateTouchBody: {
    //     flexDirection: 'row',
    //     height: 40,
    //     alignItems: 'center',
    //     justifyContent: 'center'
    // },
    dateIcon: {
        width: 32,
        height: 32,
        marginLeft: 5,
        marginRight: 5
    },
    dateInput: {
        borderWidth: 1,
        borderColor: '#005E9D',
        width: '100%',
        // minHeight: 54 * ratio,
        height: 54 * ratio,
        marginVertical: 10 * ratio,
        borderRadius: 4,
        padding: 10 * ratio,
        position: 'relative',
        alignItems:'flex-start'
    },
    // dateText: {
    //     color: '#333'
    // },
    // placeholderText: {
    //     color: '#c9c9c9'
    // },
    // datePickerMask: {
    //     flex: 1,
    //     alignItems: 'flex-end',
    //     flexDirection: 'row',
    //     backgroundColor: '#00000077'
    // },
    // datePickerCon: {
    //     backgroundColor: '#fff',
    //     height: 0,
    //     overflow: 'hidden'
    // },
    // btnText: {
    //     position: 'absolute',
    //     top: 0,
    //     height: 42,
    //     paddingHorizontal: 20,
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'center'
    // },
    // btnTextText: {
    //     fontSize: 16,
    //     color: '#46cf98'
    // },
    // btnTextCancel: {
    //     color: '#666'
    // },
    // btnCancel: {
    //     left: 0
    // },
    // btnConfirm: {
    //     right: 0
    // },
    // datePicker: {
    //     marginTop: 42,
    //     borderTopColor: '#ccc',
    //     borderTopWidth: 1
    // },
    // disabled: {
    //     backgroundColor: '#eee'
    // }
});

const DatePicker = (props) => {
    const [isFocused, setIsFocused] = useState(false)
    const [states, setStates] = useState({secureTextEntry:props.secureTextEntry})
    useEffect(() => {
        setIsFocused(!!props.value)
    }, [props.value])

    return (
        <View style={[style.container,{width:props.width || '100%'}]}>
            {/*<TextInput style={[style.input, props.style,props.error && {borderColor:'red'}]}*/}
            {/*           onFocus={setIsFocused}*/}
            {/*           onBlur={() => setIsFocused(!!props.value)}*/}
            {/*           placeholder={!isFocused ? props.placeholder : ''}*/}
            {/*           value={props.value}*/}
            {/*           onChangeText={props.onChange}*/}
            {/*           {...props}*/}
            {/*           {...states}*/}
            {/*/>*/}
            <DP style={[style.input, props.style,props.error && {borderColor:'red'}]}
                date={props.value}
                mode="date"
                placeholder={props.placeholder}
                format="YYYY-MM-DD"
                confirmBtnText={translate('confirm')}
                cancelBtnText={translate('cancel')}
                customStyles={DPStyle}
                onDateChange={(date) => props.onChange(date)}
                showIcon={false}
            />
            {isFocused && <Text style={[style.label,props.error && {color:'red'}]}>{props.placeholder}</Text>}
        </View>
    )
}

export default DatePicker;
