import React, {useEffect, useState} from "react";
import {Platform, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native'
import {ratio} from "../../utils/Styles";
import Icon from "react-native-vector-icons/Ionicons";

const style = StyleSheet.create({
    container:{
        position: 'relative',
        width:'100%'
    },
    input: {
        // flex:1,
        borderWidth: 1,
        borderColor: '#005E9D',
        width: '100%',
        // minHeight: 54 * ratio,
        height: 54 * ratio,
        marginVertical: 10 * ratio,
        borderRadius: 4,
        padding: 10 * ratio,
        position: 'relative'
    },
    label: {
        color: '#005E9D',
        position: 'absolute',
        left: 10 * ratio,
        backgroundColor: '#fff',
        padding: 5,
        top:Platform.OS === "android" ? -4 : 0
    }
})
const Input = (props) => {
    const [isFocused, setIsFocused] = useState(false)
    const [states, setStates] = useState({secureTextEntry:props.secureTextEntry})
    useEffect(() => {
        setIsFocused(!!props.value)
    }, [props.value])

    return (
        <View style={[style.container,{width:props.width || '100%'}]}>
            <TextInput style={[style.input, props.style,props.error && {borderColor:'red'}]}
                       onFocus={setIsFocused}
                       onBlur={() => setIsFocused(!!props.value)}
                       placeholder={!isFocused ? props.placeholder : ''}
                       value={props.value}
                       onChangeText={props.onChange}
                       {...props}
                       {...states}
           />
            {props.secureTextEntry && <TouchableOpacity style={{position:'absolute',right:10,top:28}} onPress={() => setStates({secureTextEntry:!states.secureTextEntry})}><Icon name={states.secureTextEntry ? "ios-eye" : "ios-eye-off"} size={20} color="#8D8D8D"/></TouchableOpacity>}
            {isFocused && <Text style={[style.label,props.error && {color:'red'}]}>{props.placeholder}</Text>}
        </View>
    )
}

export default Input;
