import React, {useEffect, useState} from "react";
import {Platform, StyleSheet, Text, View} from 'react-native'
import {ratio} from "../../utils/Styles";
import PropTypes from 'prop-types';
import RNPickerSelect from "react-native-picker-select";
import {translate} from "../../utils/translations";

const style = StyleSheet.create({
    container: {
        position: 'relative',
        width: '100%'
    },
    input: {
        // flex:1,
        borderWidth: 1,
        borderColor: '#005E9D',
        width: '100%',
        // minHeight: 54 * ratio,
        height: 54 * ratio,
        marginVertical: 10 * ratio,
        borderRadius: 4,
        padding: 10 * ratio,
        position: 'relative'
    },
    label: {
        color: '#005E9D',
        position: 'absolute',
        left: 10 * ratio,
        backgroundColor: '#fff',
        padding: 5,
        top:-4
    },

})
const rnPickerSelect = StyleSheet.create({
    inputIOSContainer: {
        paddingTop: 0,
        width: '100%',
    },
    inputAndroidContainer: {
        paddingTop: 0,
    },
    inputIOS: {
        borderWidth: 1,
        borderColor: '#005E9D',
        width: '100%',
        // minHeight: 54 * ratio,
        height: 54 * ratio,
        marginVertical: 10 * ratio,
        borderRadius: 4,
        padding: 10 * ratio,
        position: 'relative'
    },
    inputAndroid: {
        borderWidth: 1,
        borderColor: '#005E9D',
        width: '100%',
        // minHeight: 54 * ratio,
        height: 54 * ratio,
        marginVertical: 10 * ratio,
        borderRadius: 4,
        padding: 10 * ratio,
        position: 'relative'
    },
    icon: {
        borderTopWidth: 8,
        borderTopColor: '#6A6969',
        borderRightWidth: 8,
        borderLeftWidth: 8,
        top: 45,
        right: 10,
    },
    viewContainer: {
        width: '100%',
        position: 'relative',
    },
    title: {
        marginBottom: -12,
        fontWeight: 'bold',
        marginTop: 5,
        marginLeft: 10,
        top:Platform.OS === "android" ? -4 : 0
    },
})
const Select = (props) => {
    const [isFocused, setIsFocused] = useState(false)
    useEffect(() => {
        setIsFocused(!!props.value)
    }, [props.value])
    const {key,label} = props.objectRecompose
    const items = props.items.map(item => ({...item, value: item[key], label: item[label],key:item[props.key]}));
    return (
        <View style={[style.container, {width: props.width || '100%'}]}>
            <RNPickerSelect
                {...props}
                style={{...rnPickerSelect}}
                onValueChange={(value) => {
                    props.onValueChange(value)
                }}
                value={props.value}
                items={items}
                placeholder={{value: null, label: props.placeholder}}
                useNativeAndroidPickerStyle={false}

            />

            {isFocused && <Text style={[style.label, props.error && {color: 'red'}]}>{props.placeholder}</Text>}
        </View>
    )
};

Select.propTypes = {

    objectRecompose: PropTypes.shape({key:PropTypes.string,label:PropTypes.string}).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    items:PropTypes.array.isRequired,
    onValueChange:PropTypes.func.isRequired
};

export default Select;
