import React from "react";
import {ImageBackground, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import {ratio} from "../../utils/Styles";


const BlogCard = (props) => (
    <SafeAreaView style={{flex: 1, marginBottom: 10 * ratio}}>
        <TouchableOpacity style={{width: '100%'}} onPress={props.onPress}>
            {console.log(props.image)}
            <ImageBackground source={props.image} style={{width: '100%', height: 201 * ratio}} resizeMode={"cover"}>
                <View style={{
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    backgroundColor: 'rgba(0,94,157,0.93)',
                    height:100 * ratio,
                    marginTop:'auto',
                    padding:10 * ratio,
                    justifyContent:'space-between'
                }}>
                    <Text style={{color:'#fff',fontSize:16 * ratio}}>{props.title}</Text>
                    <Text style={{color:'#35BBAB',fontSize: 14 * ratio}}>{props.date}</Text>
                </View>
            </ImageBackground>

        </TouchableOpacity>

    </SafeAreaView>
);

export default BlogCard
