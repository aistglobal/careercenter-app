import React from "react";
import {SafeAreaView, View} from "react-native";


const Container = (props) => (
    <SafeAreaView style={{flex:1}}>
        <View style={[{flex: 1, justifyContent: 'center', alignItems: 'center'},props.style]}>
           {props.children}
        </View>
    </SafeAreaView>
);

export default Container
