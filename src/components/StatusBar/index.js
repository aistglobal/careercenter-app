import React from "react";
import {View,Platform,StatusBar,StyleSheet} from "react-native";
import { getStatusBarHeight } from 'react-native-status-bar-height';

const styles = StyleSheet.create({
    statusBar: {
        height: getStatusBarHeight(),
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: getStatusBarHeight(),
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
});



const CustomStatusBar = ({backgroundColor,...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
)


export default CustomStatusBar;
